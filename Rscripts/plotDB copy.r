#!/usr/bin/Rscript

#install all required packages and load them
options("repos"="http://cran.us.r-project.org")
list.of.packages <- c("ggplot2", "reshape2", "gridExtra", "grid", "RPostgreSQL", "plyr", "scales")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
lapply(list.of.packages, require, character.only=T)

cat("Fetching Sensor Data")

locIdcurrent <- 70

setwd("~/Developer/Experiments/Rscripts")

dbSensor <- data.frame()
dbPresence <- data.frame()
cacheFile <- "Sensor.txt"
cacheFile1 <- "Presence.txt"

if(1==10)
#if(!file.exists(cacheFile) || !file.exists(cacheFile1))
{
	drv <- dbDriver("PostgreSQL")
	## Open a connection
	con <- dbConnect(drv, dbname="sensordb", user="joe", password="snags98live", host="213.165.92.187", port=5432)

	options("scipen"=100)

	## Submit and execute the query
	dbSensor <- dbGetQuery(con, paste("SELECT * FROM schulversuch;", sep=""))
	dbPresence <- dbGetQuery(con, paste("SELECT * FROM \"PresenceSchoolExperiment\";", sep=""))
  
  
  ## Closes the connection
 dbDisconnect(con)
	## Frees all the resources on the driver
	dbUnloadDriver(drv)

	# we have to subtract one hour from all presence indicator readings: presence, doorcontact, windowcontact
	#dbResult[dbResult$unittypename %in% presenceFields, "timestamp"] <- dbResult[dbResult$unittypename %in% presenceFields, "timestamp"] - 1000*60*60 # subtract one hour

	# we add another column (timestamp2) to the data frame. this allows us to plot rectangles/intervals later on
	#ddplyCols <- c("loc_id", "unittypename")
	#ddplyCols <- c("locId")
	#dbResult <- ddply(dbResult, ddplyCols, function(df) {
	#	tmp <- df[with(df, order(timestamp)), setdiff(colnames(dbResult),ddplyCols)] #sort and select columns
	#	if(nrow(tmp) > 0) {
	#		tmp$timestamp2 <- tmp$timestamp[c(2:length(tmp$timestamp),length(tmp$timestamp))]
	#	}
	#	return(tmp)
	#})

	#cache locally
	write.table(dbSensor, file=cacheFile, sep="\t")
	write.table(dbPresence, file=cacheFile1, sep="\t")
} else {
	cat("Loading data from local file\n")
	dbSensor <- data.frame()
	dbSensor <- read.table(file=cacheFile, header=TRUE, sep="\t")
  dbPresence <- data.frame()
	dbPresence <- read.table(file=cacheFile1, header=TRUE, sep="\t")
}
# transform timestamps to actual times / add one column each
dbSensor$time1 <- as.POSIXct(dbSensor$timestamp, origin="1970-01-01")

dbPresence$timeFrom1 <- as.POSIXct(dbPresence$fromTime, origin="1970-01-01")
dbPresence$timeTo1 <- as.POSIXct(dbPresence$toTime, origin="1970-01-01")

#dbResult$time2 <- as.POSIXct(dbResult$timestamp2 %/% 1000, origin="1970-01-01")

  
#plotList <- lapply(unique(sensorLayerP$loc_id), function(x) { 


startDateDay <- as.POSIXlt("2014-05-06 05:00", origin="1970-01-01")
endDateDay <- startDateDay
endDateDay$hour <- endDateDay$hour + 17


#presenceOverlay <- dbPresence
#colnames(presenceOverlay)[colnames(presenceOverlay)=="unittypename"] <- "presencetype"
#presenceOverlay <- rbind.fill(lapply(c("temperature", "co2", "humidity"), function(x) data.frame(presenceOverlay, unittypename=x)))

#while(max(dbSensor$time1) > startDateDay)
#{
  sensorLayerP <- dbSensor[as.POSIXlt(dbSensor$time1) >= startDateDay & as.POSIXlt(dbSensor$time1) <= endDateDay, ]
  
  presenceOverlayP <- dbPresence[as.POSIXlt(dbPresence$timeFrom1) >= startDateDay & as.POSIXlt(dbPresence$timeFrom1) <= endDateDay, ]
  
  
  ##output
  filenameDate <- startDateDay
  #filenameDate$hour <- filenameDate$hour + 6
  filename <- paste("Schuleversuch_", format(filenameDate, "%Y-%m-%d"), ".pdf", sep="")
  print(filename)
  pdf(file=filename, width=8, height=4)
  
  
  plt <- ggplot()
  plt <- plt + geom_line(data = sensorLayerP[sensorLayerP$loc_id == locIdcurrent,], aes(x = time1, y = temperature_reading), size=0.8)
    
  ggplot(geom_line(data = sensorLayerP[sensorLayerP$loc_id == locIdcurrent,], aes(x = time1, y = temperature_reading), size=0.8))
  ggsave(test.png)  
#if(nrow(presenceOverlayP[presenceOverlayP$loc_id == locIdcurrent,]) > 0) {

    #  plt <- plt + geom_rect(data = presenceOverlayP[presenceOverlayP$loc_id == locIdcurrent ], mapping = aes(xmin = timeFrom1, xmax = timeTo1, ymin = -Inf, ymax = Inf, fill=presencetype), color=NA, alpha=0.7)
      
    #}
    #colorz <-  c("presence" = "green", "doorcontact"="red", "windowcontact"="blue", "co2"="black", "temperature"="brown", "humidity"="darkgreen")
    #plt <- plt + scale_fill_manual(values = colorz)
    #plt <- plt + scale_color_manual(values = colorz)		
    #plt <- plt + geom_line(data = sensorLayerP[sensorLayerP$loc_id == locIdcurrent,], aes(x = time1, y = temperature_reading, color=unittypename), size=0.8)
    #plt <- plt + facet_grid(loc_id+loc_displayname+unittypename~.,scales="free_y")
    #plt <- plt + guides(color="none")
    #plt <- plt + scale_x_datetime(labels = date_format("%d/%m %H:%M"), expand = c(0,0), limits = c(as.POSIXct(startDateDay), as.POSIXct(endDateDay)))
    #plt <- plt + theme_bw()
    #plt <- plt + theme(legend.position = "top", legend.title=element_blank(), panel.margin = unit(0.3, "cm"), strip.text.y = element_text(size = 8),
    #                   axis.title.y = element_text(vjust=0.25), axis.title.x = element_text(vjust=-0.05))
    print(plt)
  q#})
  dev.off()
  
  startDateDay$mday <- startDateDay$mday + 1
  endDateDay$mday <- endDateDay$mday + 1
#}
  
