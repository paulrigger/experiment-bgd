
var qs = require('qs');
var pg = require('pg');
var http = require('https');

var xivelyApiBaseUrl = 'api.xively.com';
var xivelyPath = '/v2/feeds/'
var feedId = 1928562171;
var xivelyApiKey = 'fvtrUuob4EkYqTHcKAVfbludENNMsiMU3sYoevDFhsXmhHnI';		

var conString = 'pg://node:burger89]crew@localhost:5432/sensordb';

//var client = new pg.Client(conString);
function dbQuery(queryString, callback) {
	//console.log('Q: '+queryString);
	pg.connect(conString, function(err, client, done) {
	  if(err) {
		callback(err,null);
		  }
	  client.query(queryString, function(err, result) {
		//call `done()` to release the client back to the pool
		done();

		if(err) {
			callback(err,null);
		}
		callback(null,result);
	 });
	});
}

exports.getUser = function(req, res) {
	//console.log('GET USERID by USERNAME' + req.params.userId);
	if (req.params.userId) {
		// SELECT * FROM "Users" WHERE "userId" = req.param.userId
		dbQuery('SELECT * FROM \"Users\" WHERE \"name\" = \''+req.params.userId+'\';', function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
			}	
			console.log('GET: /api/user/'+req.params.userId);
			if(result){
				if (result.rows[0]) {
					//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
					res.header("Content-Type", "application/json");
					res.jsonp(result.rows[0]);
				}
			}
			else {
				res.send(404);
				console.log('No record found');
			}	

			
		});
	}
	else {
		// SELECT * FROM user
		dbQuery('SELECT * FROM \"Users\";', function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(err);
			}	
			//console.log('GET: /api/user/');
			//console.log('CONTENT: '+JSON.stringify(result.rows));
			res.header("Content-Type", "application/json");
			res.jsonp(result.rows);
		});
	}	
}

exports.getRoom = function(req, res, next) {
	// SELECT * FROM "Users" u,"UserLocationRel" ul,"Locations" l  WHERE u."userId" = ul."userId" AND l."locId" = ul."locId";
	if (req.params.roomId) {
		dbQuery('SELECT l.\"locId" as roomId, l.\"name\" as name, l.\"category\" as category, u.\"userId\", l.\"displayName\" FROM \"Locations\" l, \"Users\" u, \"UserLocationRel\" ul' + 
		' WHERE u.\"userId\" = ul.\"userId\" AND l.\"locId\" = ul.\"locId\" AND l.\"locId\" = '+req.params.roomId+' ' + 
		' AND u.\"userId\" = '+req.params.userId+';', function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
			}	
			if (result.rows[0]) {
				//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
				res.jsonp(result.rows[0]);
			}
			else {
				res.send(404);
				console.log('No record found');
			}	
		}); 
	}
	else {
		// SELECT * FROM locations WHERE user.userId = req.param.userId AND user.locId = room.locId
		dbQuery('SELECT l.\"locId" as roomId, l.\"name\" as name, l.\"category\" as category, u.\"userId\", l.\"displayName\" FROM \"Locations\" l, \"Users\" u, \"UserLocationRel\" ul' + 
		' WHERE u.\"userId\" = ul.\"userId\" AND l.\"locId\" = ul.\"locId\"' + 
		' AND u.\"userId\" = '+req.params.userId+';', function(err, result) 
		{
		if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
			}	
			//console.log('GET: /api/user/'+req.params.userId+'/room');
			//console.log('CONTENT: '+JSON.stringify(result.rows));
			res.header("Content-Type", "application/json");
			res.jsonp(result.rows);
		}); 
	}	
}

// no last parameter
// THIS REQUEST IS TO GENERATE A GRAPH WITH HIGHCHARTS!
// Get values from the last week. Timestamp in ms for jquery!
exports.getActuatorsMeasurement = function(req, res) {
	var parameter = qs.parse(req.params.actreadings);
	//console.log('GET ACTUATORS READINGS:')
	//console.log(JSON.stringify(parameter));
	var quantity = parameter.quantity; //temperature, setpoint, valvePosition

	if (parameter.period) {
					var now = new Date();
					if (parameter.period === 'day') {
						var t1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
						t1 = t1.getTime();
						var t2 = now.getTime();
						var valveThreshold = ' ';
						// if(quantity === 'valvePosition'){
						// 	valveThreshold = ' AND ar.\"valvePosition\" > 0 ';
						// }
						dbQuery('SELECT DISTINCT ON (ar.\"timestamp\") ar.\"timestamp\", ar.\"'+quantity+'\" as value' + 
						' FROM \"Actuators\" a, \"ActuatorReadings\" ar ' + 
						' WHERE a.\"actId\" = ar.\"actId\" '+ valveThreshold +
						' AND  a.\"locId\" = '+req.params.roomId+' '+
						' AND ar.\"timestamp\" BETWEEN '+t1+' AND '+t2+
						' ORDER BY  ar.\"timestamp\" ASC',
						 function(err, result) 
						{
						if (err) {
								console.log('ERROR: '+err);
								res.send(500,err);
							}	
							//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'/actuatorsreadings/quantity='+quantity);
							//console.log('CONTENT: '+JSON.stringify(result.rows));
							//res.header("Content-Type", "application/json");
							res.jsonp(result.rows);
						}); 
					}
					else if (parameter.period === 'week') {
						var t1 = new Date (  
						    now.getFullYear(),  
						    now.getMonth(),  
						    (now.getDate()-7)  
						);
						t1 = t1.getTime();
						var t2 = now.getTime();
						dbQuery('SELECT DISTINCT ON (ar.\"timestamp\") ar.\"timestamp\", ar.\"'+quantity+'\" as value' + 
						' FROM \"Actuators\" a, \"ActuatorReadings\" ar ' + 
						' WHERE a.\"actId\" = ar.\"actId\" '+
						' AND  a.\"locId\" = '+req.params.roomId+' '+
						' AND ar.\"timestamp\" BETWEEN '+t1+' AND '+t2+
						' ORDER BY  ar.\"timestamp\" ASC',
						 function(err, result) 
						{
						if (err) {
								console.log('ERROR: '+err);
								res.send(500,err);
							}	
							//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'/actuatorsreadings/quantity='+quantity);
							//console.log('CONTENT: '+JSON.stringify(result.rows));
							//res.header("Content-Type", "application/json");
							res.jsonp(result.rows);
						}); 
					}
				}
	else {
		res.jsonp(null);
	}
}

exports.getMeasurement = function(req, res) {
	var parameter = qs.parse(req.params.measurement);
	//console.log('GET MEASUREMENTS:')
	//console.log(JSON.stringify(parameter));

	var quantity = parameter.quantity; //temperature, co2, humidity

	if(req.params.roomId !== 'all') {
		if(quantity !== 'all') {
			if (parameter.last) {
				//Example-URL:  /api/user/1/room/17/quantity=temperature&last=true

			//SELECT s."sensorId", s."currentTimestamp" as timestamp, s."currentReading" as value, u."name" as type, s."locId" as roomId FROM 
			//"Sensors" s,"UnitTypes" u  WHERE u."unitTypeId"=s."unitTypeId" AND s."locId" = 10 AND u."name" = 'radar';

				dbQuery('SELECT s.\"sensorId\", s.\"currentTimestamp\"/1000 as timestamp, s.\"currentReading\" as value, u.\"name\" as type, s.\"locId\" as roomId ' + 
				' FROM \"Sensors\" s, \"UnitTypes\" u ' + 
				' WHERE u.\"unitTypeId\" = s.\"unitTypeId\" AND  \"locId\" = '+req.params.roomId+' AND u.\"name\" = \''+quantity+'\';',
				 function(err, result) 
				{
				if (err) {
						console.log('ERROR: '+err);
						res.send(500,err);
					}	
					//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity+'last=true');
					//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
					res.header("Content-Type", "application/json");
					res.send(JSON.stringify(result.rows[0]));
				}); 
			}
			else { // no last parameter
				// THIS REQUEST IS TO GENERATE A GRAPH WITH HIGHCHARTS!
				// Get values from the last week. Timestamp in ms for jquery!
				if (parameter.period) {
					var now = new Date();
					if (parameter.period === 'day') {
						var t1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
						t1 = t1.getTime();
						var t2 = now.getTime();
						dbQuery('SELECT m.\"timestamp\", m.\"reading\" as value' + 
						' FROM \"Measurements\" m, \"Sensors\" s, \"UnitTypes\" u ' + 
						' WHERE m.\"sensorId\" = s.\"sensorId\" AND s.\"unitTypeId\" = u.\"unitTypeId\"'+
						' AND  s.\"locId\" = '+req.params.roomId+' AND u.\"name\" = \''+quantity+'\''+
						' AND \"timestamp\" BETWEEN '+t1+' AND '+t2+
						' ORDER BY  \"timestamp\" ASC'
						,
						 function(err, result) 
						{
						if (err) {
								console.log('ERROR: '+err);
								res.send(500,err);
							}	
							//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity);
							//console.log('CONTENT: '+JSON.stringify(result.rows));
							//res.header("Content-Type", "application/json");
							res.jsonp(result.rows);
						}); 
					}
					else if (parameter.period === 'week') {
						var t1 = new Date (  
						    now.getFullYear(),  
						    now.getMonth(),  
						    (now.getDate()-7)  
						);
						t1 = t1.getTime();
						var t2 = now.getTime();
						dbQuery('SELECT m.\"timestamp\", m.\"reading\" as value' + 
						' FROM \"Measurements\" m, \"Sensors\" s, \"UnitTypes\" u ' + 
						' WHERE m.\"sensorId\" = s.\"sensorId\" AND s.\"unitTypeId\" = u.\"unitTypeId\"'+
						' AND  s.\"locId\" = '+req.params.roomId+' AND u.\"name\" = \''+quantity+'\''+
						' AND \"timestamp\" BETWEEN '+t1+' AND '+t2+
						' ORDER BY  \"timestamp\" ASC',
						 function(err, result) 
						{
						if (err) {
								console.log('ERROR: '+err);
								res.send(500,err);
							}	
							//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity);
							//console.log('CONTENT: '+JSON.stringify(result.rows));
							//res.header("Content-Type", "application/json");
							res.jsonp(result.rows);
						}); 
					}
				}
				else {
					dbQuery('SELECT m.\"timestamp\", m.\"reading\" as value' + 
					' FROM \"Measurements\" m, \"Sensors\" s, \"UnitTypes\" u ' + 
					' WHERE m.\"sensorId\" = s.\"sensorId\" AND s.\"unitTypeId\" = u.\"unitTypeId\"'+
					' AND  s.\"locId\" = '+req.params.roomId+' AND u.\"name\" = \''+quantity+'\''+
					' ORDER BY m.\"timestamp\" DESC LIMIT 288;',
					 function(err, result) 
					{
					if (err) {
							console.log('ERROR: '+err);
							res.send(500,err);
						}	
						//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity);
						//console.log('CONTENT: '+JSON.stringify(result.rows));
						//res.header("Content-Type", "application/json");
						res.jsonp(result.rows);
					}); 
				}
			} 
		}
		else {
			if (parameter.last) {

				dbQuery('SELECT s.\"sensorId\", s.\"currentTimestamp\"/1000 as timestamp, s.\"currentReading\" as value, u.\"name\" as type, s.\"locId\" as roomId ' + 
				' FROM \"Sensors\" s, \"UnitTypes\" u ' + 
				' WHERE u.\"unitTypeId\" = s.\"unitTypeId\" AND  \"locId\" = '+req.params.roomId+';',
				 function(err, result) 
				{
				if (err) {
						console.log('ERROR: '+err);
						res.send(500,err);
					}	
					//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity+'last=true');
					//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
					res.header("Content-Type", "application/json");
					res.send(JSON.stringify(result.rows));
				}); 
			}
			else{
				res.send('Invalid URL2: '+req);
			}
		} 
	}
	else {
		if(quantity ==='all'){
			if(parameter.last){
				//Example-URL:  /api/user/1/room/all/quantity=all&last=true
				//SELECT s."sensorId", s."currentTimestamp"/1000 as timestamp, s."currentReading" as value, u."name" as type, s."locId" as roomId, th."min", th."max"  
				//FROM "Sensors" s, "UnitTypes" u,  "Users" us, "UserLocationRel" ul, "Locations" l,  "Thresholds" th  WHERE u."unitTypeId"= s."unitTypeId" 
				//AND s."unitTypeId"=th."unitTypeId" AND us."userId"=2 AND s."locId" = ul."locId" AND ul."userId" = us."userId"  AND l."locId" = s."locId" AND th."category" = l."category"
				dbQuery('SELECT s.\"sensorId\", s.\"currentTimestamp\"/1000 as timestamp, s.\"currentReading\" as value, u.\"name\" as type, s.\"locId\" as roomId, th.\"min\", th.\"max\" ' + 
				' FROM \"Sensors\" s, \"UnitTypes\" u,  \"Users\" us, \"UserLocationRel\" ul, \"Locations\" l,  \"Thresholds\" th ' +
				' WHERE u.\"unitTypeId\"= s.\"unitTypeId\" AND s.\"unitTypeId"\=th.\"unitTypeId\" AND us.\"userId\"='+req.params.userId+' AND s.\"locId\" = ul.\"locId\" AND ul.\"userId\" = us.\"userId\" ' + 
				' AND l.\"locId\" = s.\"locId\" AND th.\"category\" = l.\"category\" ;',
				 function(err, result) 
				{
				if (err) {
						console.log('ERROR: '+err);
						res.send(500,err);
					}	
					//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity+'last=true');
					//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
					res.header("Content-Type", "application/json");
					res.send(JSON.stringify(result.rows));
				}); 
			}
		}
	}
}
	

exports.addUser = function(req, res) {
	console.log(req);
	console.log(req.body);
	var name = req.body.name + req.body.surname;
	// INSERT INTO "Users" VALUES  (DEFAULT,'Werner')
	dbQuery(	'INSERT INTO \"Users\" '+ 
						' VALUES (DEFAULT, \''+name+'\');',
		function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
			}	
			//console.log('POST: /api/user/');
			//console.log('CONTENT: '+req.body);
			res.send(result);
		});
}

exports.addRoom = function(req, res) {
	var userId = req.body.userId;
	var name = req.body.name;
	var category = req.body.category;
	dbQuery(	'INSERT INTO \"Locations\"' + ' VALUES (DEFAULT, \''+name+'\') returning \"locId\" ;',
		function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
			}
			//console.log('returning locId: ', result.rows[0]);
			dbQuery('INSERT INTO \"UserLocationRel\" VALUES ('+userId+','+result.rows[0].locId+');',
				function(err2, result2) {
					if (err2) {
						console.log('ERROR: '+err2);
						res.send(500,err2);
					}		
					//console.log('POST: /api/user/'+userId+'/room');
				//	console.log('CONTENT: '+req.body);
					res.send(result2);
				});
		}); 
}



exports.addMeasurement = function(req, res) {
	var userId = req.body.userId;
	var roomId;
	if(req.body.roomId) {
		roomId = req.body.roomId;
	}
	else if(req.body.roomid) {
		roomId = req.body.roomid;
	}
	else {
		console.log('Error: No roomid or roomId found');
	}

	var ofMeasurements = req.body.measurements;
	//console.log(ofMeasurements);
	var data = "";
	
	function iterateMeasurements(ofMeasurements, callback) {
		console.log('DEBUG: addMeasurement: locId: '+roomId);
		dbQuery('SELECT \"sensorId\" FROM \"Sensors\" WHERE \"unitTypeId\" <= 5 AND \"locId\" = '+roomId+' ORDER BY \"unitTypeId\";',  
		function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
			}
			var datastreams = [];
			var xivelyJSON = {};
			var datapointsTemp = [];
			var datapointsHumidity = [];
			var datapointsCO2 = [];
			var datapointsPressure = [];
			var datapointsSound = [];

			ofMeasurements.forEach(function(measurement,index) {
			var timestamp = measurement.timestamp*1000;
			var sensorId;
			if(result.rows) {
			  if(measurement.type === 'temperature') {
				if(result.rows[0]){	
					sensorId = result.rows[0].sensorId;
					var date = new Date(timestamp).toISOString(); // Xively wants a date string
					datapointsTemp.push({'at':date+'','value':measurement.value+''}); // concerns Xively
				}
			   }
			  else if(measurement.type === 'humidity') {
				if(result.rows[1]) {
					sensorId = result.rows[1].sensorId;
					var date = new Date(timestamp).toISOString(); // Xively wants a date string
					datapointsHumidity.push({'at':date+'','value':measurement.value+''}); // concerns Xively
				}   
			}
			  else if(measurement.type === 'co2') {
				if(result.rows[2]){
					sensorId = result.rows[2].sensorId;
					var date = new Date(timestamp).toISOString(); // Xively wants a date string
					datapointsCO2.push({'at':date+'','value':measurement.value+''}); // concerns Xively
				}
			  }
			  else if(measurement.type === 'pressure') {
				if(result.rows[3]){
					sensorId = result.rows[3].sensorId;
					var date = new Date(timestamp).toISOString(); // Xively wants a date string
					datapointsPressure.push({'at':date+'','value':measurement.value+''}); // concerns Xively
				}
			  }
			  else if(measurement.type === 'sound') {
				if(result.rows[4]){
					sensorId = result.rows[4].sensorId;
					var date = new Date(timestamp).toISOString(); // Xively wants a date string
					datapointsSound.push({'at':date+'','value':measurement.value+''}); // concerns Xively
				}
			  }
			  else {
				console.log('Rows:', JSON.stringify(result.rows));
				console.log('Measurement Type unkown! ', measurement.type);  
			  }
			  if(sensorId){
				data += '(DEFAULT, '+measurement.value+', '+timestamp+', '+sensorId+'),';
				//console.log('Measurement added to data: ', measurement);
			  }
			  else {
				//console.log('No SensorId for '+ req.originalUrl);
			  }
			}
			});
			callback(data);
	 	});
	}
	iterateMeasurements(ofMeasurements,function(data) {
		if(data) {			
		data = data.slice(0,-1); // remove the last comma
		//console.log('--------- INSERTION TO DB -------------');
		dbQuery('INSERT INTO \"Measurements\" VALUES '+data+';',
		function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
				console.log('--------- INSERTION ERROR TO DB !!! -------------');

			}	
			//console.log('POST: /api/user/'+userId+'/room/'+roomId+'/measurement');
			//console.log('CONTENT: '+req.body);
			res.send(200,'OK');
			console.log('-----------DB INSERTION SUCCESSFUL-----------');
		}); 
		}
		else {
			console.log('No DATA inserted in DB');
			res.send(200,'No data inserted');
		}

	})
}

exports.addThreshold = function(req, res) {
	var userId = req.body.userId;
	var roomId = req.body.roomId;
	var min = req.body.min;
	var max = req.body.max;
	var type = req.body.type;
	connection.query(	'INSERT threshold'+ 
								' VALUES (null, \''+type+'\', '+min+', '+max+', '+roomId+');',
		function(err, result, fields) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
			}	
			//console.log('POST: /api/user/'+userId+'/room/'+roomId+'/treshold');

			//console.log('CONTENT: '+req.body);
			res.send(result);
		}); 
}


exports.updateCategory = function(req, res) {
	console.log('updateCategory');
	var userId = req.body.userId;
	var roomId = req.body.roomId;
	var category = req.body.category;
	
	connection.query(	'UPDATE room'+ 
						' SET category = \''+category+'\' WHERE roomId = '+roomId+' AND userId = '+userId+';',
		function(err, result, fields) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
			}	
			//console.log('UPDATE catetogry');

			//console.log('CONTENT: '+req.body);
			res.send(result);
		}); 
}
