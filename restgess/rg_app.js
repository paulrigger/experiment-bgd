

var express = require('express'),
    http = require('http');

var app = express();
app.configure(function () {
  app.set('port', 8080);
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});
var server = http.createServer(app);
var io = require('socket.io').listen(server);

// Here the actual database requests are handled
var rg = require('./restgress.js');
var qh = require('./quantifiedhome.js');
var an = require('./analytics.js');


var messenger = require('./messenger.js');

server.listen(app.get('port'));
io.set('log level',1) // set log level to get all debug messages
io.on('connection',function(socket){
  socket.emit('init',{msg:"test"});
  socket.on('identify', function(mac, fn) {
    rg.getGatewayByMac(mac, function(err,gatewayId) {
        if (err) {
            fn(null);
        }
        else {
            fn(gatewayId);
        }

    })
  })
  socket.on('trv_readings',function(readings) {
    rg.addActMeasurements(readings)
  })
  socket.on('tempOffsets', function(gatewayId, fn) {
    rg.getTempOffsets(gatewayId,function(err, objsOffsets) {
        console.log(JSON.stringify(objsOffsets))
        if (err) {
            console.log('Error in socket.on(\'tempOffsets\')');
            fn(objsOffsets);
        }
         else {
            fn(objsOffsets);
        }
    })
  })
  socket.on('preHeatSlopes', function(gatewayId, fn) {
    rg.getMinutesForOneDegreeByGatewayId(gatewayId, function(err, objsPreHeatSlopes) {
        console.log(JSON.stringify(objsPreHeatSlopes))
        if (err) {
            console.log('Error in socket.on(\'preHeatSlopes\')');
            fn(objsPreHeatSlopes);
        }
         else {
            fn(objsPreHeatSlopes);
        }    
    })
  })  
})



/* 
    DISTANCE CHANGE NOTIFIER NEEDS TO BE REFACTORED
*/

var distChangeListener = rg.pgListenClient;
distChangeListener.connect();
distChangeListener.query('LISTEN "distance"');
distChangeListener.on('notification', function(msg) {
    console.log(msg.channel);
    console.log(msg.payload);
     if (msg.channel === 'distance') {
        if (msg.payload > 0) {
            var payload = (msg.payload).split(':');
            var userId = parseInt(payload[0]);
            var distance = parseFloat(payload[1]);
            console.log('DISTANCE userId:' +userId);
            rg.getGatewayByUserId(userId, function(err, gatewayId) {
                if (err) {
                    console.log('getGatewayByUserId Error: '+err);
                } 
                rg.getLocIdsByUserId(userId, function(err, objLocIds) {
                if (err) {
                    console.log('getLocIdsByUserId Error: '+err);
                }
                    objLocIds.forEach(function(objLocId) {
                        rg.getCurrentAutoAwayOffset(objLocId.locId, function(err,offset) {
                            if (err) {
                                console.log('getCurrentAutoAwayOffset Error: '+err);
                            }
                            if (offset !== null) {
                                data = {
                                    gatewayId: gatewayId,
                                    roomId: objLocId.locId,
                                    offset: offset  
                                };
                                messenger.send('autoaway',data);    
                            }            
                        })
                    })    
                })   
            })
        }
    }   
});
/***********************************************************************************/


messenger.receive(function(topic,message) {
    var gatewayId = topic[1];

    console.log('Message received with topic: '+topic+' and message: '+message);
    if (message == 'online' || message == 'request_update') {

        /* STILL ONE USER PER GATEWAY */
        rg.getUsersByGatewayId(gatewayId, function(err, objsUserId) {
            objsUserId.forEach(function(objUserId) {
                var userId = objUserId.userId;
                rg.getRoomsWithActuatorsByUserId(userId, function(err,rooms) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        rooms.forEach(function(room) {
                            rg.getGeneralMode(room.roomid, function(err, gmode) {
                                if (err) {
                                    console.log(err);
                                }
                                if (gmode ==2) {
                                    rg.getCurrentAutoAwayOffset(room.roomid, function(err,offset) {
                                        if (err) {
                                            console.log('getCurrentAutoAwayOffset Error: '+err);
                                        }
                                        if (offset !== null) {
                                            data = {
                                                gatewayId: gatewayId,
                                                roomId: room.roomid,
                                                offset: offset  
                                            };
                                            console.log(JSON.stringify(data));
                                            messenger.send('autoaway',data);    
                                        }            
                                    })
                                }
                            })
                            rg.getScheduleByLocId(room.roomid, function(err,schedule) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    var data = {
                                        gatewayId: gatewayId,
                                        roomId: room.roomid,
                                        body: schedule
                                    };
                                    messenger.send('schedule',data);
                                }
                            })
                            rg.getSetpoints(room.roomid, function(err,setpoints) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    var data = {
                                        gatewayId: gatewayId,
                                        roomId: room.roomid,
                                        body: setpoints
                                    };
                                    messenger.send('setpoints',data);
                                }
                            })
                            rg.getActuatorsByLocId(room.roomid, function(err,actuators) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    var names = [];
                                    actuators.forEach(function(actuator) {
                                        names.push({
                                            actId: actuator.actId,
                                            name: actuator.name
                                        });
                                    });
                                    var data = {
                                        gatewayId: gatewayId,
                                        roomId: room.roomid,
                                        body: names
                                    };
                                    messenger.send('actuators',data);
                                }
                            })
                        })
                    }
                })    
            })
                
        })   
       
    }

    else if (message == 'ping') {
        rg.updateOnlineState(gatewayId, function(err,res) {
            if (err) console.log('updateOnlineState Error: '+err);
        })
    }   
    if (topic[2] == 'setpoint') {
        var message = message.split(':');
        var locId = message[0];
        var mode = message[1]
        var setpoint = message[2];
        rg.postSingleSetpoint(locId, setpoint, mode);
    }
});

/*
	GET - REST Endpoints
*/
app.get('/api',function(req, res) {
	res.send(200,'API up and running...');
});

app.get('/api/time',rg.getTime);

/*
    Software update
*/
app.get('/api/update',function(req,res) {
    io.sockets.emit('update')
    res.send(200);
});

app.get('/api/user/:userId/update',function(req,res) {
    var userId = parseInt(req.params.userId);
    rg.getGatewayByUserId(userId, function(err,gatewayId) {
        if (err) {
            res.send(500,err);
        }
        else {
            messenger.send('update',gatewayId);
            res.send(200);
        }    
    });
});    

app.get('/api/onlinestatus',function(req,res) {
    rg.getOnlineState(0, function(err,objOnState) {
        if (err) {
            res.send(500,err);
        }
        res.send(200,objOnState);
    })
})

app.get('/api/user/:userId/onlinestatus',function(req, res) {
    var userId = parseInt(req.params.userId);
    rg.getGatewayByUserId(userId, function(err,gatewayId) {
        if (err) {
            res.send(500,err);
        }
        else {
            rg.getOnlineState(gatewayId, function(err,objOnState) {
                if (objOnState[0]) {
                    res.send(200,objOnState[0]);
                }
                else res.send(404);
            })
        }
    })
});


//app.get('/api/',qh.getIndex);
app.get('/api/user/:userId?',qh.getUser);
app.get('/api/user/:userId/room/:roomId?',qh.getRoom);
/*
        Options for parameter measurement:
        quantity                                                                : get all records of quantity
        quantitiy?last=true                                     : get only most recent record of quantity
        quantity?from=timestamp                                 : get records from timestamp until most recent
        quantity?to=timestamp                                   : get records until timestamp
        quantity?from=timestamp1&to=timestamp2  : get records between timestamp1 and timestamp2
*/
app.get('/api/user/:userId/room/:roomId/schedule',rg.getSchedule);

app.get('/api/user/:userId/room/:roomId/actuators', function(req,res) {
    var locId = req.params.roomId;
    rg.getActMeasurements(locId, function(err,actValues) {
        if (err) {
            res.send('500',err);
        }
        else {
            res.send('200',actValues);
        }
    });
});

app.get('/api/user/:userId/room/:roomId/sensors',rg.getSensors);

// app.get('/api/user/:userId/room/:roomId/status',function(req, res) {
//     var userId = req.params.userId;
//     var locId = req.params.roomId;
//     rg.getSetpoints(locId, function(err,setpoints) {
//         if (err) res.send(500,err)
//         else {    
//             rg.getCurrentModeWithSetpoint(locId, function(err, obj) {
//                 //if (err) res.send(500,err)
//                 if (typeof obj !== 'undefined' && obj !== null ) {
//                 var mode = obj.modeId;
//                 }
//                 else mode = null;
//                 if (setpoints === null || setpoints === undefined) {
//                     var status = {};
//                 }
//                 else {    
//                     var status = setpoints;
//                 }
//                 status.roomid = locId;
//                 //status.setpointComfort = setpoints.setpointComfort;
//                 console.log('FOUND STATUS MODE:' +mode);
//                 if (mode === null || mode === undefined) {
//                     status.mode = 9;
//                 }
//                 else {
//                     status.mode = mode;    
//                 }

//                 if (status.mode === 0) {
//                     status.msgTemperature = 'Der Raum wird auf deine Komfort-Temperatur geregelt';
//                     //status.setpointComfort = obj.setpoint;
//                 }
//                 else if (status.mode === 1) {
//                     status.msgTemperature = 'Der Raum wird auf deine Nacht-Temperatur geregelt';
//                     //status.setpointNight = obj.setpoint;
//                 }
//                 else if (status.mode === 2 ){
//                     status.msgTemperature = 'Der Raum wird auf deine Unterwegs-Temperatur runtergeregelt';
//                     status.setpointAway = parseInt(obj.setpoint);
//                 }
//                 else {
//                     status.msgTemperature = 'Die Temperatur im Raum ist angenehm warm';

//                 }
//                 console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'/status')
                
//                 an.getHumidityMsg(userId,locId, function(result) {
//                     status.msgHumidity = result;
//                     an.getCo2Msg(userId,locId, function(result) {
//                         status.msgCo2 = result;
//                         res.send(status)
//                         console.log(status)
//                     });
//                 });
//             });
//         }
//     })        
// });


app.get('/api/user/:userId/room/:roomId/status',function(req, res) {
    var userId = req.params.userId;
    var locId = req.params.roomId;
            var status ={}
            console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'/status')
            an.getTemperatureStatusAndMsg(userId,locId,function(result) {
                status = result;
                an.getHumidityMsg(userId,locId, function(result) {
                    status.msgHumidity = result;
                    an.getCo2Msg(userId,locId, function(result) {
                        status.msgCo2 = result;
                        res.send(status)
                        console.log(status)
                    });
                });
        });
});


app.get('/api/user/:userId/room/:roomId/heatuptime',function(req, res) {
    var locId = req.params.roomId;
    rg.getHeatUpTime(locId,function(err,time) {
        if (err) {
            res.send('500',err);
        }
        else {
            res.send('200',{
                roomId: parseInt(locId),    
                heatUpTime: time});
        }
    });    
});

app.get('/api/user/:userId/room/:roomId/actuatorsreadings/:actreadings',qh.getActuatorsMeasurement);

/*
Returns the current setpoints of a room together with the average setpoints of the room's category
*/
app.get('/api/user/:userId/room/:roomId/avgsetpoints',rg.getAvgSetpoints);


app.get('/api/user/:userId/room/:roomId/:measurement',qh.getMeasurement);



app.post('/api/user/:userId/gpslocations',rg.addGpsLocation);


app.post('/api/user/:userId/:locId/ibeacons',rg.addIBeaconData);

app.get('/api/user/:userId/:locId/ibeacons/:status',rg.addIBeaconDataGET);

//app.get('/api/user/:userId/:measurement',qh.getMeasurement);
//app.get('/api/user/:userId/room/:roomId/threshold/:quantitiy?',qh.getThreshold);
//app.get('/api/feedback/user/:userId/from/:from/to/:to',qh.getFeedback);

//app.post('/api/quantifiedhome/updatecategory',qh.updateCategory);
app.get('/api/test/wemo/on',function(req,res) {
    messenger.send('wemo','1');
    res.send(200);
});
app.get('/api/test/wemo/off',function(req,res) {
    messenger.send('wemo','0'),
    res.send(200);
});


/*
	POST - REST Endpoints
*/


/*
        POST to add new measurements
        {
                s1values       	: [<float>,..],
                t1values        : [<UNIXtime>,...],
		...
		...
		s28values        : [<float>,..],
                t28values        : [<UNIXtime>,...]
        }
*/
//app.post('/api/measurements',rg.addMeasurement);

/*
 	POST to add new measurements
	{	
		timestamp	: <UNIXtime>,
		value		: <float>
	}
*/
 app.get('/api/sensors/:sensorId/measurements',rg.getMeasurementForSensor);
 app.post('/api/sensors/:sensorId/measurements',rg.addMeasurementForSensor);


/*
        POST to add a new user
        {       surname : <String>,
                name    : <String>      }
*/
 app.post('/api/user',qh.addUser);
/*
        POST to add a new room for an user
        {       userId          : <Int>,
                name            : <String>,
                category        : <String>      }
*/
 app.post('/api/user/:userId/room',qh.addRoom);


app.get('/api/user/:userId/pairing',messenger.send('pairing'),function(req,res) {
    res.send(200);
});
/*
        POST to add new measurements
        {       userId          : <Int>,
        roomId          : <Int>,
        measurements: <Array>
                                {       timestamp       : <UNIXtime>,
                                        value           : <Float>,
                                        type            : <String: temperature, humidity, co2>  }
*/
 app.post('/api/user/:userId/room/:roomId/measurement',qh.addMeasurement);
/*
        POST to add new thresholds
        {       userId          : <Int>
                roomId          : <Int>,
                min                     : <Float>,
                max                     : <Float>,
                type            : <String: temperature, humidity, co2>  }
*/
 app.post('/api/user/:userId/room/:roomId/threshold',qh.addThreshold);


app.post('/api/user/:userId/room/:roomId/schedule',messenger.send('schedule'),rg.postSchedule);

app.post('/api/user/:userId/room/:roomId/setpoints',messenger.send('setpoints'),rg.postSetpoints);


/*
    General mode
*/
app.post('/api/user/:userId/room/:roomId/mode',messenger.send('mode'),function(req,res) {
    var locId = req.params.roomId;
    var gmode = req.body.mode;
    //console.log('General mode: '+gmode);
    rg.setGeneralMode(locId,gmode,function(err,result) {
        if (err) {
            res.send(500,err);
        }
        else res.send(200);
    })    
});



