var rg = require('./restgress')
var mqtt = require('mqtt')
  , client = mqtt.createClient(5672,'localhost');



client.on('connect',function() {
	console.log('Messenger connected');
  client.subscribe('Backend/#');
});  

exports.receive = function(callback) {
	client.on('message', function(topic, message) {
      var topic = topic.split('/');
    	callback(topic, message);

  	});
};

exports.send = function(type,opt) {

  if (opt) {
    if (type == 'update') {
      var gatewayId = opt;
      client.publish(gatewayId+'/'+type);
      console.log('Message sent: '+gatewayId+'/'+type);  
    }
    else if (type == 'wemo') {
      client.publish('/meem/Socket-1_0-221310K1101673/binary/in/'+opt);
      console.log('Message sent: /meem/Socket-1_0-221310K1101673/binary/in/'+opt);
    }
    else {
      if (type == 'autoaway') {
        opt.body = opt.offset;
      }
      client.publish(opt.gatewayId+'/'+opt.roomId+'/'+type,JSON.stringify(opt.body));
      console.log('Message sent: '+opt.gatewayId+'/'+opt.roomId+'/'+type);
    }  
  } 
  else { 
    return function(req, res, next) {
            rg.getGatewayByUserId(req.params.userId, function(err, gatewayId) { 
              if (err) console.log(err);
              if (req.params.roomId && req.body) {
                client.publish(gatewayId+'/'+req.params.roomId+'/'+type,JSON.stringify(req.body));
                console.log('Message sent: '+gatewayId+'/'+req.params.roomId+'/'+type);
              }
              else {
                client.publish(gatewayId+'/0/'+type,null);
                console.log('Message sent: '+gatewayId+'/0/'+type);  
              }  
        		  next();
            })  
    }
  }      
};
