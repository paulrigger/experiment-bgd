
var rg = require('./restgress.js');

var inEnglishLanguage = false;

exports.getHumidityMsg = function(userId, locId, callback) {
	var msg = 'Aktuell keine Informationen verfügbar. Sorry!';
	if(inEnglishLanguage){
		msg = 'Sorry, no information available';
	}

	rg.getMeasurement(userId,locId, function(measurement) 
	{
		// Idee 1: Abfrage der Outdoor Humidity um Stosslüften zu empfehlen!
		// Idee 2: Abfrage der Zeit seitdem diese geringe Humidity anliegt um spezifischer die Message zu gestalten.
		if(measurement){
		if(measurement.humidity) {
			//console.log('Humidity value: '  + measurement.humidity);
			var value = measurement.humidity;
			if(measurement.category === 'Outdoor'){
					msg = 'Relative Luftfeuchtigkeit draussen';
				if(inEnglishLanguage) {
					msg = 'Outdoor relative humidity'
				}
				callback(msg);
			}
			else {
			if(value <= 30) 
			{
				msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich niedrig. Dauerhaft trockene Luft ist ungesund.';
				if(inEnglishLanguage) {
					msg = 'Humidity is currently too low. Permanently dry air is unhealthy.'
				}
				rg.getLocForUserAndCategory(userId,'Outdoor', function(locIdOutdoor) {
						if(locIdOutdoor){
						console.log('LocationId of outdoor found: ' + locIdOutdoor);
							rg.getMeasurement(userId,locIdOutdoor.locId, function(measurementOutdoor) {
								if(measurementOutdoor){
								 var outdoorWaterAmount = getMaxAmountOfWaterInAirInGrammPerCubicMetre(measurementOutdoor.temperature);
								 var indoorWaterAmount = getMaxAmountOfWaterInAirInGrammPerCubicMetre(measurement.temperature);
								 var humidityOutdoor = measurementOutdoor.humidity;
								 var humidityIndoor = humidityOutdoor * outdoorWaterAmount / indoorWaterAmount;
								 var humIndoor = Math.round(humidityIndoor);
								 console.log('Predicted Humidity Indoors: ' + humidityIndoor);
								 if(humidityIndoor > value) {
								 	 msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich niedrig. Dauerhaft trockene Luft ist ungesund. Die Luft draußen enthält heute mehr Luftfeuchtigkeit, deswegen ist ein kurzes Stosslüften zu empfehlen';
								 	 if(inEnglishLanguage) {
										msg = 'Humidity is currently too low. Permanently dry air is unhealthy. The outside air contains much more humidity, thus short airing is recommended.'
									 }
								 }
								 else {
								 	msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich niedrig. Dauerhaft trockene Luft ist ungesund. Lüften hilft aber aktuell nicht, da die Aussenluft bei einer Temperatur von ' +measurementOutdoor.temperature+ ' Grad weniger Feuchtigkeit enthält';
								 	 if(inEnglishLanguage) {
										msg = 'Humidity is currently too low. Permanently dry air is unhealthy. However airing does not help, since the outside air contains less humidity at a temperature of ' +measurementOutdoor.temperaure+ ' degrees.';
									 }
								 }
								 callback(msg);
								}
								else {
										msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich niedrig. Dauerhaft trockene Luft ist ungesund.';
									if(inEnglishLanguage) {
										msg = 'Humidity is currently too low. Permanently dry air is unhealthy.';
									 }
									callback(msg);
								}

							});
						}
						else {
							msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich niedrig. Dauerhaft trockene Luft ist ungesund.';
							if(inEnglishLanguage) {
										msg = 'Humidity is currently too low. Permanently dry air is unhealthy.';
									 }
							callback(msg);
						}
					});

			}
			else if(value <= 35 && value > 30) 
			{
				msg = 'Die Luftfeuchtigkeit ist etwas niedrig aber noch in Ordnung.';
				if(inEnglishLanguage) {
										msg = 'Humidity is low, but still okay.';
									 }
				callback(msg);
			}
			else if(value > 35 && value <= 65) 
			{
				msg = 'Die Luftfeuchtigkeit ist optimal.';
				if(inEnglishLanguage) {
										msg = 'Humidity is optimal.';
									 }
				callback(msg);
			}
			else if(value > 65 && value < 70) 
			{
				msg = 'Die Luftfeuchtigkeit ist etwas hoch aber noch in Ordnung.';
				if(inEnglishLanguage) {
										msg = 'Humidity is high, but still ok.';
									 }
				callback(msg);
			}
			else if(value >= 70) 
			{
				msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich hoch, was u.a. zur Schimmelbildung führen kann.';
				if(inEnglishLanguage) {
										msg = 'Humidity is currently too high, which can lead to mouldiness.';
									 }
				rg.getLocForUserAndCategory(userId,'Outdoor', function(locIdOutdoor) {
						if(locIdOutdoor){
						console.log('LocationId of outdoor found: ' + locIdOutdoor);
							rg.getMeasurement(userId,locIdOutdoor.locId, function(measurementOutdoor) {
								if(measurementOutdoor){
								 var outdoorWaterAmount = getMaxAmountOfWaterInAirInGrammPerCubicMetre(measurementOutdoor.temperature);
								 var indoorWaterAmount = getMaxAmountOfWaterInAirInGrammPerCubicMetre(measurement.temperature);
								 var humidityOutdoor = measurementOutdoor.humidity;
								 var humidityIndoor = humidityOutdoor * outdoorWaterAmount / indoorWaterAmount;
								 var humIndoor = Math.round(humidityIndoor);
								 console.log('Predicted Humidity Indoors: ' + humidityIndoor );
								 if(humidityIndoor < value) {
								 	msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich hoch, was u.a. zur Schimmelbildung führen kann. Die Luft draußen enthält deutlich weniger Feuchtigkeit, deswegen kurz Stosslüften.';
								 	if(inEnglishLanguage) {
										msg = 'Humidity is currently too high, which can lead to mouldiness. The outside air contains less humidity, thus short airing is recommended.';
									}

								 }
								 else {
								 	msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich hoch, was u.a. zur Schimmelbildung führen kann. Lüften ist aber nicht sinnvoll, da die Luft draussen bei einer Temperatur von '+measurementOutdoor.temperature+' mehr Feuchtigkeit enthält.';
								 	if(inEnglishLanguage) {
										msg = 'Humidity is currently too high, which can lead to mouldiness. Airing does not help, since the outside air contains even more humidity at a temperature of '+measurementOutdoor.temperature+'';
									}
								 }
								 callback(msg);
								}
								else {
									msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich hoch, was u.a. zur Schimmelbildung führen kann.';
									if(inEnglishLanguage) {
										msg = 'Humidity is currently too high, which can lead to mouldiness.';
									}
									callback(msg);
								}

							});
						}
						else {
							msg = 'Die Luftfeuchtigkeit ist aktuell ziemlich hoch, was u.a. zur Schimmelbildung führen kann.';
							if(inEnglishLanguage) {
										msg = 'Humidity is currently too high, which can lead to mouldiness.';
									}
							callback(msg);
						}
					});
			}
			}							
		}
		callback(msg);
		}
		
	});
}


getMaxAmountOfWaterInAirInGrammPerCubicMetre = function(temperature) {
	if(temperature <= -10) {
		return 2.14;
	}
	else if(temperature <= -5) {
		return 3.24;
	}
	else if(temperature <= 0) {
		return 4.84;
	}
	else if(temperature <= 2) {
		return 5.6;
	}
	else if(temperature <= 4) {
		return 6.4;
	}
	else if(temperature <= 6) {
		return 7.3;
	}
	else if(temperature <= 8) {
		return 8.3;
	}
	else if(temperature <= 10) {
		return 9.4;
	}
	else if(temperature <= 12) {
		return 10.7;
	}
	else if(temperature <= 14) {
		return 12.1;
	}
	else if(temperature <= 16) {
		return 13.6
	}
	else if(temperature <= 18) {
		return 15.4;
	}
	else if(temperature <= 20) {
		return 17.3;
	}
	else if(temperature <= 22) {
		return 19.4;
	}
	else if(temperature <= 24) {
		return 21.8;
	}
	else if(temperature <= 26) {
		return 24.4;
	}
	else if(temperature <= 28) {
		return 27.2;
	}
	else if(temperature <= 30) {
		return 30.3;
	}
	else if(temperature <= 32) {
		return 33.8;
	}
	else if(temperature <= 34) {
		return 37.5;
	}
	else {
		return 40;
	} 

}

exports.getCo2Msg = function(userId, locId, callback) {
	var msg = 'Aktuell keine Informationen verfügbar. Sorry!';
	if(inEnglishLanguage){
		msg = 'Sorry, no information available';
	}

	rg.getMeasurement(userId,locId, function(measurement) 
	{
		if(measurement){
		if(measurement.co2) {
			var co2 = measurement.co2;
			if(co2 > 2000) {
				rg.getAvgCo2PerMinute(locId,'FALSE',function(value) {
					if(value) {
					var co2PerMin = value.co2permin;
					var airingTime = Math.round(((co2 - 800) / co2PerMin) / 2);
					msg = 'Der CO2-Gehalt ist hoch, was auf eine schlechte Luftqualität hindeutet. Dauerhaft kann das zur Müdigkeit oder leichten Kopfschmerzen führen, deswegen für etwa '+airingTime+' Minuten lüften';
					if(inEnglishLanguage){
						msg = 'The CO2 concentration is high, which indicates a bad air quality. This can lead to tiredness or headaches. We recommend an airing for '+airingTime+' minutes.';
					}
					callback(msg);	
					}
				});
			}
			else if(co2 > 1000) {
				rg.getAvgCo2PerMinute(locId,'FALSE',function(value) {
					if(value) {
					var co2PerMin = value.co2permin;
					// console.log('--->co2permin: '+ co2PerMin);
					// console.log('--->co2-500: '+ (co2-500));
					// console.log('---minutes: '+  ((co2-500)/co2PerMin));

					var airingTime = Math.round(((co2 - 800) / co2PerMin) / 3);
					msg = 'Der CO2-Gehalt ist etwas hoch, was auf eine nicht optimale Luftqualität hindeutet. Einfach kurz für '+airingTime+' Minuten lüften';
					if(inEnglishLanguage){
						msg = 'The CO2 concentration is high, which indicates a bad air quality. We recommend an airing for '+airingTime+' minutes.';
					}
					callback(msg);	
					}
				});
			}
			else if (co2 > 750) {
					msg = 'Die Luftqualität ist nicht optimal aber ausreichend gut';
					if(inEnglishLanguage){
						msg = 'The air quality is not optimal, but ok.';
					}
					callback(msg);	
			}
			else {
				msg = 'Die Luftqualität ist sehr gut';
				if(inEnglishLanguage){
						msg = 'The air quality is optimal.';
				}
				callback(msg);	
			}
		}
		else {
			callback(msg);
		}
		}
	});
}


exports.getTemperatureStatusAndMsg = function(userId, locId, callback) {
		var msg = 'Aktuell keine Informationen verfügbar. Sorry!';
		if(inEnglishLanguage){
		msg = 'Sorry, no information available';
		}
		var status = { roomId: locId,
						gmode: 0,
						mode: 9,
					 };
		rg.getCurrentModeWithSetpoint(locId, function(err, status4) {
			if(err) {
				console.log('----> GetCurrentModeWithSetpoints failed: '+err);
			}
			else if(status4) {
				rg.getSetpoints(locId, function(err,setpoints) {
        			if (err) {
        				console.log('GetSetpoints failed: '+err);
        				status.msgTemperature = msg;
        				callback(status);
        			}
        			else {
        				status.gmode = status4.generalmodeId;
        				status.mode = status4.modeId;
        				status.setpointComfort = setpoints.setpointComfort;
        				status.setpointNight = setpoints.setpointNight;
        				status.setpointAway = setpoints.setpointAway;
        				if(status.mode === 2) {
        					if(status4.setpoint){
        						status.setpointAway = parseFloat(status4.setpoint);
        					}
        				}
        				status.msgTemperature = msg;

        				rg.getMinutesForOneDegree(locId,'TRUE', function(value) {
        				var degreeDiff = status4.setpoint - status4.temperature;
        				console.log('degreeDiff: '+ degreeDiff);
        				var minPerDegree = 40;
        				var heatuptime = 0;
        				if(degreeDiff >= 0.3) {
        					if(value) {
        						minPerDegree = value.minperdegree;
        						console.log('minPerDegree: '+ minPerDegree);
        					}
        					if(minPerDegree <= 10 || minPerDegree >= 240) {
        						minPerDegree = 40;
        					}
        					heatuptime = Math.round(degreeDiff*minPerDegree);
        					console.log('heatuptime: '+heatuptime);
        				}

        				//NORMAL
        				if(status.gmode === 1){
        					var basicMsg = '';
							if (status.mode === 0) {
                    			basicMsg = 'Nach Zeitplan wird der Raum auf deine Comfort-Temperatur von '+status4.setpoint+'° geregelt.';
                    			if(inEnglishLanguage){
									basicMsg = 'According to your schedule,\nthe room is set to your comfort temperature of '+status4.setpoint+'°';
								}
			                }
               				else if (status.mode === 1) {
                   				basicMsg = 'Nach Zeitplan wird der Raum auf deine Nacht-Temperatur von '+status4.setpoint+'° geregelt.';
                   				if(inEnglishLanguage){
									basicMsg = 'According to your schedule,\nthe room is set to your night temperature of '+status4.setpoint+'°';
								}
			                }
              				else if (status.mode === 2 ){
                    			basicMsg = 'Nach Zeitplan wird der Raum zum Energiesparen auf deine Unterwegs-Temperatur von '+status4.setpoint+'° geregelt.';
                    			if(inEnglishLanguage){
									basicMsg = 'According to your schedule,\nthe room is set to your away temperature of '+status4.setpoint+'°';
								}
                			}  
                			if(heatuptime > 0) {
                				status.msgTemperature = basicMsg + '\nDas dauert etwa '+heatuptime+ ' Minuten.';
                				if(inEnglishLanguage){
                				status.msgTemperature = basicMsg + '\nThis takes about '+heatuptime+ ' minutes.';
                				}      				
                			}
                			else {
                				status.msgTemperature = basicMsg;      				
                			}
                			callback(status);
                		}
        				//AutoAway
        				else if(status.gmode === 2){
        					rg.getDistance(userId, function(err, dist) {
        					var distance = -1;
        					var basicMsg = '';
        					if(dist){
        						distance = Math.round(dist);
        					}
							if (status.mode === 0) {
                    			basicMsg = 'Home Sweet Home, der Raum wird auf deine Comfort-Temperatur von '+status4.setpoint+'° geregelt.';
                    			if(inEnglishLanguage){
                					status.msgTemperature = basicMsg + '\nHome Sweet Home, the room is set up to your comfort temperature of '+status4.setpoint+'°';
                				}  
			                }
               				else if (status.mode === 1) {
                   				basicMsg = 'Gute Nacht, der Raum wird auf deine Nacht-Temperatur von '+status4.setpoint+'° geregelt.';
                   				if(inEnglishLanguage){
                					basicMsg = '\nGood night, the room is set to your night temperature of '+status4.setpoint+'°';
                				}
			                }
              				else if (status.mode === 2 ){
              					if(distance >= 0) {
                    				basicMsg = 'Du bist unterwegs und etwa '+distance+' Kilometer von deiner Wohnung entfernt.\nDer Raum wird zum Energiesparen auf '+status4.setpoint+'° geregelt.';
        							if(inEnglishLanguage){
        							basicMsg = 'You are more than '+distance+' km away from home. The room is set to '+status4.setpoint+'° for saving energy.';
        						    }
        						}
        						
                			}  
                			if(heatuptime > 0) {
                				status.msgTemperature = basicMsg + '\nDas dauert etwa '+heatuptime+ ' Minuten.';
                				if(inEnglishLanguage){
                				status.msgTemperature = basicMsg + '\nThis takes about '+heatuptime+ ' minutes.';
                				}        				
                			}
                			else {
                				status.msgTemperature = basicMsg;      				
                			}
                			callback(status);
                			});    				
        				}
        				//Urlaub
        				else if(status.gmode === 3 ) {
        					 status.msgTemperature = 'Du bist länger unterwegs.\nDer Raum wird zum Energiesparen auf deine Unterwegs-Temperatur runtergeregelt.';  
        					 if(inEnglishLanguage){
                				status.msgTemperature = 'You are out of home for a while. \n The room is set to your away temperature for saving energy.';
                				}  
        					 callback(status);    				
        				}
      
        				});
        			}
        		});
			}
			else 
			{
				rg.getMeasurement(userId,locId, function(measurement) {
					if(measurement){
						console.log('---------->>>>>>> OUTDOOR CHECK ----');
						console.log(measurement.category);
						if(measurement.category === 'Outdoor'){
							status.msgTemperature = 'Aussentemperatur';
							if(inEnglishLanguage){
                				status.msgTemperature = 'Outdoor temperature';
                				} 
							console.log('Outdoor');
							callback(status);
						}
						else
						{
						if(measurement.temperature) {
						var temp = measurement.temperature;
						if(temp > 24) {
							status.msgTemperature = 'Die Temperatur im Raum ist relativ hoch.';  
							 if(inEnglishLanguage){
                				status.msgTemperature = 'The room temperature is relatively high.';
                				}  
						}
						else if(temp >=20 && temp <=24) {
							status.msgTemperature = 'Die Temperatur im Raum ist angenehm warm.';  
							if(inEnglishLanguage){
                				status.msgTemperature = 'The room temperature is comfortable.';
                				}  
						}
						else if(temp >=18 && temp <=20) {
							status.msgTemperature = 'Die Temperatur im Raum ist ok.';  
							if(inEnglishLanguage){
                				status.msgTemperature = 'It is a bit cool in here.';
                				} 
						}
						else if (temp < 18) {
							status.msgTemperature = 'Die Temperatur im Raum ist relativ niedrig.';  
							if(inEnglishLanguage){
                				status.msgTemperature = 'It is a bit cool in here.';
                				} 
						}
						else {
							status.msgTemperature = 'Aktuell keine Informationen verfügbar. Sorry!'; 
							if(inEnglishLanguage){
								msg = 'Sorry, no information available';
							}
						}
							}
							callback(status);
						}
					}	
				});
			}
		});
}

















