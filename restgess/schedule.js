var _ = require('underscore');

Date.prototype.subHours= function(h){
    this.setHours(this.getHours()-h);
    return this;
}

exports.cirqlTime = function() {
	var date 	= new Date();
	var cDate	= new Date();
	var day 	= cDate.subHours(3).getDay();
	var hour 	= date.getHours();
	var minute 	= date.getMinutes();
	if (day == 0) day = 7;
	return {
		day: 	day, 
		hour: 	hour,
		minute: minute
	};
}

function toTime(jsonTime) {
	var hhmm = jsonTime.split(':');
	var time = parseInt(hhmm[0])*100+parseInt(hhmm[1]);
	return time	
}

function toJsonTime(time) {
	var inttime = time / 100;
	var jsonTime = {};
	var hh = Math.floor(inttime)+'';
	if (hh.length == 1) hh = '0'+ hh;
	var mm = (time % 100)+'';
	if (mm.length == 1) mm = mm + '0';
	return hh+':'+mm
}


exports.toPostgresSchedule = function(schedule, locId, callback) {
	var data = '';
	if (schedule.generalMode) {
		var gmode = schedule.generalMode;
	}
	else {
		gmode = 1;
	}
	
	schedule.ofWeekIntervals.forEach(function(weekInterval) {
		//if (weekInterval.end === 0) {
		//	weekInterval.end = 7;
		//}
		//var days = _.range(weekInterval.begin, weekInterval.end+1);
		//console.log(days)
		//console.log(days.length)
		
		weekInterval.ofDayIntervals.forEach(function(dayInterval) {
			var beginTime 	= toTime(dayInterval.begin);
			var endTime 	= toTime(dayInterval.end);


			//for (var i = 0; i < days.length; i++) {
			//	if (days[i] == 7) {
			//		days[i] = 0;
			//	}
				data += '(DEFAULT,'+locId+','+beginTime+','+endTime+','+dayInterval.mode+',\''+weekInterval.name+'\','+gmode+'),';
			//};

		})
	})
	data = data.slice(0,-1); // remove the last comma
	callback(data);
}



exports.toJsonSchedule = function(schedule, callback) {
	var ofWeekIntervals = [];
	var ofDayIntervals = [];
	var ofWeekDayIntervals = [];
	var ofWeekEndDayIntervals = [];

	schedule.rows.forEach(function(row) {

		if (ofWeekIntervals[0] === undefined) {
			ofWeekIntervals[0] = new Object;
		}

		if (row.name === 'Mo-Fr') {
			ofWeekDayIntervals.push({
				mode: row.modeId,
				begin: toJsonTime(row.begin),
				end: toJsonTime(row.end)
			})
			//ofDayIntervals = _.uniqObjects(ofDayIntervals);
			ofWeekIntervals[0].ofDayIntervals = ofWeekDayIntervals;
			ofWeekIntervals[0].name = 'Mo-Fr';
			ofWeekIntervals[0].begin = 1;
			ofWeekIntervals[0].end = 5;
		}
		else if (row.name === 'Sa-So') {
			ofWeekEndDayIntervals.push({
				mode: row.modeId,
				begin: toJsonTime(row.begin),
				end: toJsonTime(row.end)
			})
			//ofDayIntervals = _.uniqObjects(ofDayIntervals);
			if (ofWeekIntervals[1] === undefined) {
				ofWeekIntervals[1] = new Object;
			}
			ofWeekIntervals[1].ofDayIntervals = ofWeekEndDayIntervals;
			ofWeekIntervals[1].name = 'Sa-So';
			ofWeekIntervals[1].begin = 6;
			ofWeekIntervals[1].end = 7;
		}
		else if (row.name === 'Mo-So') {
			ofDayIntervals.push({
				mode: row.modeId,
				begin: toJsonTime(row.begin),
				end: toJsonTime(row.end)
			})
			ofWeekIntervals[0].ofDayIntervals = ofDayIntervals;
			ofWeekIntervals[0].name = 'Mo-So';
			ofWeekIntervals[0].begin = 1;
			ofWeekIntervals[0].end = 7;
		}


	})
	callback(ofWeekIntervals)
}



