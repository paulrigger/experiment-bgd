var qs = require('qs'); 
var pg = require('pg'); 
var http = require('https');

var xivelyApiBaseUrl = 'api.xively.com';
var xivelyPath = '/v2/feeds/';
var feedId = 1928562171;
var xivelyApiKey = 'fvtrUuob4EkYqTHcKAVfbludENNMsiMU3sYoevDFhsXmhHnI';		

var conString = 'pg://node:burger89]crew@localhost:5432/qadb';

//var client = new pg.Client(conString);
function dbQuery(queryString, callback) {
	//console.log('Q: '+queryString);
	pg.connect(conString, function(err, client, done) {
	  if(err) {
		callback(err,null);
		  }
	  client.query(queryString, function(err, result) {
		//call `done()` to release the client back to the pool
		//done();

		if(err) {
			callback(err,null);
		}
		callback(null,result);
	 });
	});
}

exports.addMeasurement = function(req, res) {
	var userId = req.body.userId;

	
	var ofMeasurements = req.body.measurements;
	//console.log(ofMeasurements);
	var data = "";
	
	function iterateMeasurements(ofMeasurements, callback) {
		console.log('DEBUG: addMeasurement: Id: '+userId);
		

			ofMeasurements.forEach(function(measurement,index) {
				var timestamp = measurement.timestamp;
				
				var temperature = measurement.temperature+'';
				var humidity = measurement.humidity+''; // concerns Xively
				var co2 = measurement.co2+''; // concerns Xively
				
				data += '(DEFAULT, '+ userId+', '+timestamp+', '+temperature+', '+humidity+', '+co2+'),';
				
				
			});
		callback(data);
	}
			

	iterateMeasurements(ofMeasurements,function(data) {
		if(data) {			
		data = data.slice(0,-1); // remove the last comma
		console.log('--------- INSERTION TO DB -------------');
		console.log('INSERT INTO \"measurements\" VALUES '+data+';');
		dbQuery('INSERT INTO \"measurements\" VALUES '+data+';',
		function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
				console.log('--------- INSERTION ERROR TO DB !!! -------------');

			}	
			//console.log('POST: /api/user/'+userId+'/room/'+roomId+'/measurement');
			//console.log('CONTENT: '+req.body);
			res.send(200,'OK');
			console.log('-----------DB INSERTION SUCCESSFUL-----------');
		}); 
		}
		else {
			console.log('No DATA inserted in DB');
			res.send(200,'No data inserted');
		}

	});
};

exports.getMeasurement = function(req, res) {
	var parameter = qs.parse(req.params.measurement);

	dbQuery('SELECT * FROM measurements WHERE qaid = '+ req.params.userId + ';', 
		function(err,result){
			if(err){
				console.log('ERROR: '+err);
				res.send(500,err);
			}	
				//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity+'last=true');
				//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
			res.header("Content-Type", "application/json");
			res.send(JSON.stringify(result));
			}
	);

};

/*
exports.getMeasurement = function(req, res) {
	var parameter = qs.parse(req.params.measurement);
	//console.log('GET MEASUREMENTS:')
	//console.log(JSON.stringify(parameter));

	var quantity = parameter.quantity; //temperature, co2, humidity

	if(req.params.roomId !== 'all') {
		if(quantity !== 'all') {
			if (parameter.last) {
				//Example-URL:  /api/user/1/room/17/quantity=temperature&last=true

			//SELECT s."sensorId", s."currentTimestamp" as timestamp, s."currentReading" as value, u."name" as type, s."locId" as roomId FROM 
			//"Sensors" s,"UnitTypes" u  WHERE u."unitTypeId"=s."unitTypeId" AND s."locId" = 10 AND u."name" = 'radar';

				dbQuery('SELECT s.\"sensorId\", s.\"currentTimestamp\"/1000 as timestamp, s.\"currentReading\" as value, u.\"name\" as type, s.\"locId\" as roomId ' + 
				' FROM \"Sensors\" s, \"UnitTypes\" u ' + 
				' WHERE u.\"unitTypeId\" = s.\"unitTypeId\" AND  \"locId\" = '+req.params.roomId+' AND u.\"name\" = \''+quantity+'\';',
				 function(err, result) 
				{
				if (err) {
						console.log('ERROR: '+err);
						res.send(500,err);
					}	
					//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity+'last=true');
					//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
					res.header("Content-Type", "application/json");
					res.send(JSON.stringify(result.rows[0]));
				}); 
			}
			else { // no last parameter
				// THIS REQUEST IS TO GENERATE A GRAPH WITH HIGHCHARTS!
				// Get values from the last week. Timestamp in ms for jquery!
				if (parameter.period) {
					var now = new Date();
					if (parameter.period === 'day') {
						var t1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
						t1 = t1.getTime();
						var t2 = now.getTime();
						dbQuery('SELECT m.\"timestamp\", m.\"reading\" as value' + 
						' FROM \"Measurements\" m, \"Sensors\" s, \"UnitTypes\" u ' + 
						' WHERE m.\"sensorId\" = s.\"sensorId\" AND s.\"unitTypeId\" = u.\"unitTypeId\"'+
						' AND  s.\"locId\" = '+req.params.roomId+' AND u.\"name\" = \''+quantity+'\''+
						' AND \"timestamp\" BETWEEN '+t1+' AND '+t2+
						' ORDER BY  \"timestamp\" ASC'
						,
						 function(err, result) 
						{
						if (err) {
								console.log('ERROR: '+err);
								res.send(500,err);
							}	
							//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity);
							//console.log('CONTENT: '+JSON.stringify(result.rows));
							//res.header("Content-Type", "application/json");
							res.jsonp(result.rows);
						}); 
					}
					else if (parameter.period === 'week') {
						var t1 = new Date (  
						    now.getFullYear(),  
						    now.getMonth(),  
						    (now.getDate()-7)  
						);
						t1 = t1.getTime();
						var t2 = now.getTime();
						dbQuery('SELECT m.\"timestamp\", m.\"reading\" as value' + 
						' FROM \"Measurements\" m, \"Sensors\" s, \"UnitTypes\" u ' + 
						' WHERE m.\"sensorId\" = s.\"sensorId\" AND s.\"unitTypeId\" = u.\"unitTypeId\"'+
						' AND  s.\"locId\" = '+req.params.roomId+' AND u.\"name\" = \''+quantity+'\''+
						' AND \"timestamp\" BETWEEN '+t1+' AND '+t2+
						' ORDER BY  \"timestamp\" ASC',
						 function(err, result) 
						{
						if (err) {
								console.log('ERROR: '+err);
								res.send(500,err);
							}	
							//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity);
							//console.log('CONTENT: '+JSON.stringify(result.rows));
							//res.header("Content-Type", "application/json");
							res.jsonp(result.rows);
						}); 
					}
				}
				else {
					dbQuery('SELECT m.\"timestamp\", m.\"reading\" as value' + 
					' FROM \"Measurements\" m, \"Sensors\" s, \"UnitTypes\" u ' + 
					' WHERE m.\"sensorId\" = s.\"sensorId\" AND s.\"unitTypeId\" = u.\"unitTypeId\"'+
					' AND  s.\"locId\" = '+req.params.roomId+' AND u.\"name\" = \''+quantity+'\''+
					' ORDER BY m.\"timestamp\" DESC LIMIT 288;',
					 function(err, result) 
					{
					if (err) {
							console.log('ERROR: '+err);
							res.send(500,err);
						}	
						//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity);
						//console.log('CONTENT: '+JSON.stringify(result.rows));
						//res.header("Content-Type", "application/json");
						res.jsonp(result.rows);
					}); 
				}
			} 
		}
		else {
			if (parameter.last) {

				dbQuery('SELECT s.\"sensorId\", s.\"currentTimestamp\"/1000 as timestamp, s.\"currentReading\" as value, u.\"name\" as type, s.\"locId\" as roomId ' + 
				' FROM \"Sensors\" s, \"UnitTypes\" u ' + 
				' WHERE u.\"unitTypeId\" = s.\"unitTypeId\" AND  \"locId\" = '+req.params.roomId+';',
				 function(err, result) 
				{
				if (err) {
						console.log('ERROR: '+err);
						res.send(500,err);
					}	
					//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity+'last=true');
					//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
					res.header("Content-Type", "application/json");
					res.send(JSON.stringify(result.rows));
				}); 
			}
			else{
				res.send('Invalid URL2: '+req);
			}
		} 
	}
	else {
		if(quantity ==='all'){
			if(parameter.last){
				//Example-URL:  /api/user/1/room/all/quantity=all&last=true
				//SELECT s."sensorId", s."currentTimestamp"/1000 as timestamp, s."currentReading" as value, u."name" as type, s."locId" as roomId, th."min", th."max"  
				//FROM "Sensors" s, "UnitTypes" u,  "Users" us, "UserLocationRel" ul, "Locations" l,  "Thresholds" th  WHERE u."unitTypeId"= s."unitTypeId" 
				//AND s."unitTypeId"=th."unitTypeId" AND us."userId"=2 AND s."locId" = ul."locId" AND ul."userId" = us."userId"  AND l."locId" = s."locId" AND th."category" = l."category"
				dbQuery('SELECT s.\"sensorId\", s.\"currentTimestamp\"/1000 as timestamp, s.\"currentReading\" as value, u.\"name\" as type, s.\"locId\" as roomId, th.\"min\", th.\"max\" ' + 
				' FROM \"Sensors\" s, \"UnitTypes\" u,  \"Users\" us, \"UserLocationRel\" ul, \"Locations\" l,  \"Thresholds\" th ' +
				' WHERE u.\"unitTypeId\"= s.\"unitTypeId\" AND s.\"unitTypeId"\=th.\"unitTypeId\" AND us.\"userId\"='+req.params.userId+' AND s.\"locId\" = ul.\"locId\" AND ul.\"userId\" = us.\"userId\" ' + 
				' AND l.\"locId\" = s.\"locId\" AND th.\"category\" = l.\"category\" ;',
				 function(err, result) 
				{
				if (err) {
						console.log('ERROR: '+err);
						res.send(500,err);
					}	
					//console.log('GET: /api/user/' +req.params.userId+ '/room/'+req.params.roomId+'quantity='+quantity+'last=true');
					//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
					res.header("Content-Type", "application/json");
					res.send(JSON.stringify(result.rows));
				}); 
			}
		}
	}
}
*/



