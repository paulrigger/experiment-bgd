
var qs = require('qs');
var pg = require('pg');
var http = require('https');
var Schedule = require('./schedule.js');

/*
 UNCOMMENT FOR AXON BASED MESSAGING
*/
// var axon = require('axon')
//   , sock = axon.socket('push'); 

// sock.bind(5672,'0.0.0.0');


var xivelyApiBaseUrl = 'api.xively.com';
var xivelyPath = '/v2/feeds/'
var feedId = 1928562171;
var xivelyApiKey = 'fvtrUuob4EkYqTHcKAVfbludENNMsiMU3sYoevDFhsXmhHnI';			

var conString = 'pg://node:burger89]crew@localhost:5432/sensordb';


/******************************************
	LISTEN for postgres messages
*/
exports.pgListenClient = new pg.Client(conString);

/*****************************************/


//var client = new pg.Client(conString);
function dbQuery(queryString, callback) {
	pg.connect(conString, function(err, client, done) {
	  if(err) {
	    callback(err,null);
	   	  }
	  client.query(queryString, function(err, result) {
    	//call `done()` to release the client back to the pool
    	//console.log('Q: '+queryString);
    	done();

    	if(err) {
    		console.log('E: '+err);
    		callback(err,null);
    	}
    	//console.log('A: '+ JSON.stringify(result));
    	callback(null,result);
	 });
	});
}	

exports.getTime = function(req, res) {
	var time = new Date();
	time = time+''
	res.send(time);
}


exports.getSensors = function(req, res) {
	var roomId = req.params.roomId;
	//console.log('Get Measurement for sensor: '+ sensorId)
	dbQuery('SELECT * ' + 
				' FROM \"Sensors\" s ' + 
				' WHERE s.\"locId\"='+roomId+';',
			    function(err,result) {
				if (err) {
					console.log('postgress error',err);
					res.send(500,err);
				}
				//console.log("result: " + result.rows[0]);
				res.jsonp(200, result.rows);
			   });
}

exports.getMeasurementForSensor = function(req, res) {
	var sensorId = req.params.sensorId;
	console.log('Get Measurement for sensor: '+ sensorId)
	dbQuery('SELECT * ' + 
				' FROM \"Sensors\" s ' + 
				' WHERE s.\"sensorId\"='+sensorId+';',
			    function(err,result) {
				if (err) {
					console.log('postgress error',err);
					res.send(500,err);
				}
				//console.log("result: " + result.rows[0]);
				res.jsonp(200, result.rows[0]);
			   });
}



exports.addMeasurementForSensor = function(req, res) {
	console.log('addMeasurement for sensor ' + req.params.sensorId);
	var sensorId = req.params.sensorId;
	var measurements = req.body.measurements;
    var data ="";

    console.log('sensorId: ' + sensorId);
    console.log('measurements: ' + JSON.stringify(measurements));

// {
// sensorId: 86,
// measurements: 
// 	[ 
//   		{ timestamp: 1397663157, value: 1, type: 'kwhTick' },
//   		{ timestamp: 1397663157, value: 10, type: 'waterVolumeTick' }
// 	]
// }

   function iterateMeasurements(ofMeasurements, callback) {
   ofMeasurements.forEach(function(measurement, index){
   	var timestamp = measurement.timestamp*1000;
   	console.log('timestamp: ' + timestamp);
   	data += '(DEFAULT, '+measurement.value+', '+timestamp+', '+sensorId+'),';
   });
   callback(data);
   };
   
   	iterateMeasurements(measurements,function(data) {
		if(data) {			
		data = data.slice(0,-1); // remove the last comma
		console.log('+++--------- INSERTION TO DB -------------');
		dbQuery('INSERT INTO \"Measurements\" VALUES '+data+';',
		function(err, result) {
			if (err) {
				console.log('ERROR: '+err);
				res.send(500,err);
				console.log('+++--------- INSERTION ERROR TO DB !!! -------------');

			}	
			//console.log('POST: /api/user/'+userId+'/room/'+roomId+'/measurement');
			//console.log('CONTENT: '+req.body);
			res.send(200,'OK');
			console.log('+++-----------DB INSERTION SUCCESSFUL-----------');
		}); 
		}
		else {
			console.log('+++No DATA inserted in DB');
			res.send(200,'+++No data inserted');
		}
	});
}

// var measurementCount = 0;
// var numberOfMeasurementsInPOST = 20;
// var datastreams = [];
// var xivelyJSON = {};
// var add = true;
// exports.addMeasurement = function(req, res) {
// 	console.log('addMeasurement of restgress.js is called');
// 	var body = req.body;
// 	var count = Object.keys(body).length;
// 	console.log('Count: ', count);
// 	var valStr = 't';
// 	var timeStr = 't';
// 	var vals; 
// 	var times;
// 	var data = '';
// 	measurementCount = measurementCount+1;
// 	console.log('MeasurementCount: '+measurementCount);

// 	for (var i = 1; i < 29; i++) {
// 		var datapoints = [];
// 		data = '';
// 		valStr = 's'+ i + 'values';
// 		if(body.hasOwnProperty(valStr)){

// 			timeStr = 's'+i+'times';
// 			if(body.hasOwnProperty(timeStr)){

// 				vals = body[valStr];
// 				times = body[timeStr];
// 				for (var j = 0; j < vals.length; j++ ) {
// 					var sensorId = i;
// 		            data += '('+vals[j]+', '+times[j]+','+sensorId+'),';
// 		            var date = new Date(times[j]).toISOString(); // Xively wants a date string
// 		            datapoints.push({'at':date+'','value':vals[j]+''}); // concerns Xively
//                 }
//                 data = data.slice(0,-1); // remove comma
//                 add = true;
//                 datastreams.forEach(function(entry){
//                 	if (entry.id === 'sensor'+i) {
//                 		entry.datapoints = entry['datapoints'].concat(datapoints);
//                 		add = false;
//                 		return 
//                 	}
//                 })
//                 if (add === true) {
// 	                datastreams.push({
// 	                			'id':'sensor'+i,
// 	                			'datapoints':datapoints});	
//              	}
                
// 				var queryString = 'INSERT INTO \"Measurements\" (\"reading\",\"timestamp\",\"sensorId\") VALUES '+data;
// 				//console.log('Query: ', queryString);
//         			dbQuery(queryString, function(err,result) {
//                 		if (err) {
//                         		console.log('postgress error',err)
//                 		}
// 				//console.log('---');
// 				});		
// 			}
// 		}
// 	}

//     if (measurementCount == numberOfMeasurementsInPOST) {
//                 	measurementCount = 0;
// 					xivelyJSON = {
// 	                	'version':'1.0.0',
// 	                	'datastreams' : datastreams
// 	                };
// 	                datastreams = [];
// 	                add = true;
// 	                xivelyJSON = JSON.stringify(xivelyJSON);
// 	                /*
// 	                console.log("---------------------------------")
// 	                console.log("JSON sent to Xively:")
// 	                console.log("---------------------------------")
// 	                console.log(xivelyJSON);
// 	                console.log("---------------------------------")
// 					*/
// 	                var headers = {
// 	  					'X-ApiKey' : xivelyApiKey,
// 	  					'Content-Type': 'application/json',
// 	  					'Content-Length': xivelyJSON.length
// 					};

// 					var options = {
// 					  host: xivelyApiBaseUrl,
// 					  path: xivelyPath+feedId,
// 					  method: 'PUT',
// 					  headers: headers
// 					};

// 					var req = http.request(options, function(res) {
// 					  	res.setEncoding('utf8');
// 	  					console.log('Status Code: ' + res.statusCode);
// 	  					console.log('Headers:');
// 	  					console.log(res.headers);
// 					});

// 					req.on('error', function(e) {
// 						console.log("---------------------------------")
// 						console.log('Xively POST Error:')
// 						console.log("---------------------------------")
// 						console.log(e);
// 						console.log("---------------------------------")
// 					});

// 					req.write(xivelyJSON);
// 					req.end();	
//     }
// 	res.send('test');
// }

exports.addMeasurementGET = function(req, res) {
	console.log('URL: ', req.url);
	var sensorId = req.params.sensorId;
	console.log('sensorid: ', sensorId);
	console.log('values: ',req.query.v);
	console.log('timestamps: ', req.query.t);
	var values = req.query.v;
	var timestamps = req.query.t;
	var data = '';
	var time = new Date().getTime();
	var newtimestamp = 0;
	
	if(req.query.v instanceof Array){
		for (var i = 0; i < values.length; i++ ) {
			newtimestamp = time - timestamps[i];
			data += '('+values[i]+', '+newtimestamp+','+sensorId+'),';
		}
		data = data.slice(0,-1); // remove the last comma
	}
	else {
                  data += '('+values+', '+time+','+sensorId+')';
	}
	//console.log('Data: ' + data);
	queryString = 'INSERT INTO \"Measurements\" (\"reading\",\"timestamp\",\"sensorId\") VALUES '+data;
	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('postgress error',err)
		}	
		res.send('OK');
	})

}

exports.addActMeasurements = function(readings) {
	dbQuery('SELECT a.\"actId\"'+
	' FROM \"Actuators\" a'+
	' WHERE a.\"name\" =\''+readings.actuator+'\';',
		function(err, result) 
		{
			if (err) {
				console.log('ERROR: '+err);
			}
			else if (typeof result === 'undefined' || result.rows[0] == undefined) {
				console.log('Actuator not found');
			}
			else {
				var actId = result.rows[0].actId;
				if (!readings.offset) {
					readings.offset = 0;
				}
				var queryString = 'INSERT INTO \"ActuatorReadings\"'+
					' (\"setpoint\",\"temperature\",\"valvePosition\",\"timestamp\",\"actId\",\"offset\")'+
					' VALUES ('+readings.setpoint+','+readings.temperature+','+readings.valveposition+','+readings.timestamp+','+actId+','+readings.offset+');';
				dbQuery(queryString, function(err,result) {
					if (err) {
						console.log('postgress error',err);
					}
					//console.log('postgress: ',result);			
				});
			}
		}
	)	
}

exports.getActMeasurements = function(locId,callback) {
	dbQuery('SELECT a.\"name\", a.\"setpoint\", a.\"temperature\", a.\"valvePosition\", a.\"timestamp\" '+
	' FROM \"Actuators\" a'+
	' WHERE a.\"locId\" = '+locId+';',
		function(err, result) 
		{
			if (err) {
				console.log('ERROR: '+err);
				callback(err,null)
			}
			else if (typeof result === 'undefined' || result.rows[0] == undefined) {
				console.log('Get Actuator Meausrements failed');
				callback(null,null);
			}
			else {
				callback(null,result.rows);
			}
		});
}

exports.getCurrentModeWithSetpoint = function(locId, callback) {

	dbQuery('SELECT s.\"generalmodeId\", s.\"modeId\", s.\"setpoint\", s.\"temperature\"'+
	' FROM \"status4\" s'+
	' WHERE s.\"locid\" ='+locId+' LIMIT 1;',
		function(err, result) 
		{
			if (err) {
				console.log('ERROR: '+err);
				callback(err,null)
			}
			else if (typeof result === 'undefined' || result.rows[0] == undefined) {
				callback(null,null);
			}
			else {
				callback(null,result.rows[0])
			}
		}
	); 	
}



exports.getSetpoints = function(locId, callback) {
	if(locId !== 'all'){
	//Example-URL:  /api/user/1/room/14/settings/all

	//SELECT s."setpointComfort", s."setpointNight", s."setpointAway", s."locId" as roomId FROM 
	//"Settings" s  WHERE s."locId" = 10;

	dbQuery('SELECT s.\"setpointComfort\", s.\"setpointNight\", s.\"setpointAway\", s.\"locId\" as roomId'+
	' FROM \"Settings\" s'+
	' WHERE s.\"locId\" ='+locId+
	' ORDER BY s.\"settingsId\" DESC LIMIT 1;',
		function(err, result) 
		{
			if (err) {
				console.log('ERROR: '+err);
				callback(err,null)
			}
			else {
				callback(null,result.rows[0])
			}
		}); 

	} 
}

exports.getCurrentAutoAwayOffset = function(locId, callback) {
	var queryString = 'SELECT \"redOffset\" as offset FROM \"status4\" WHERE \"locid\" ='+locId+' AND \"generalmodeId\"=2;';
	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('postgress error',err);
			callback(err,null);
		}
		else if (typeof result === 'undefined' || result.rows[0] == undefined) {
			callback(null,null);
		}
		else {
			//console.log('postgress: ',result);
			//console.log('RESULT  CURRENT AUTOAWAY OFFSET: '+result.rows[0]);
			callback(null,result.rows[0])
		}
	})	
}

// Concerning schedule
exports.getMode = function(locId, callback) {
	var now = Schedule.cirqlTime();
	var hhmm = now.hour*100+now.minute+'';
	var intervalName = 'Mo-Fr';
	if(hhmm < 300) {
		hhmm = 2400 + hhmm;
	}
	if(now.day <= 5) {
		intervalName = 'Mo-Fr';
	}
	else {
		intervalName = 'Sa-So';
	}
	var queryString = 'SELECT S1.\"modeId\" FROM '+
	 '((SELECT \"locId\", \"begin\"+2400 as begin, \"end\"+2400 as end, \"modeId\", \"name\" FROM \"Schedules\"  WHERE \"begin\" < 300 AND \"end\" < 300) UNION ' + 
	 '(SELECT \"locId\", \"begin\", \"end\"+2400 as end, \"modeId\", \"name\" FROM \"Schedules\"  WHERE \"begin\" >= 300 AND \"end\" < 300) UNION ' +
	 '(SELECT \"locId\", \"begin\", \"end\", \"modeId\", \"name\" FROM \"Schedules\"  WHERE \"begin\" >=300 AND \"end\" >=300)) S1 '+ 
	 'WHERE \"locId\" ='+locId+' AND \"name\" = \''+intervalName+'\' AND \"begin\" < '+hhmm+' AND \"end\" >='+hhmm+';';
		console.log(queryString)
	dbQuery(queryString, function(err,result) {
		if (err || result === undefined) {
			console.log('postgress error',err);
			callback(err,null);
		}
		else {
		//console.log('postgress: ',result);
		//console.log('RESULT MODE: '+result.rows[0]);
		callback(null,result.rows[0])
		}
	})		
}

exports.getGeneralMode = function (locId, callback) {
	var queryString = 'SELECT \"currentMode\" FROM \"Locations\" WHERE \"locId\" ='+locId+';';
	dbQuery(queryString, function(err,result) {
		if (err || result === undefined) {
			console.log('postgress error',err);
			callback(err,null);
		}
		else {
		//console.log('postgress: ',result);
		//console.log('RESULT  GENERAL MODE: '+result.rows[0].currentMode);
		callback(null,result.rows[0].currentMode)
		}
	})	
}
getGeneralMode = function (locId, callback) {
	var queryString = 'SELECT \"currentMode\" FROM \"Locations\" WHERE \"locId\" ='+locId+';';
		console.log(queryString)
	dbQuery(queryString, function(err,result) {
		if (err || result === undefined) {
			console.log('postgress error',err);
			callback(err,null);
		}
		else {
		//console.log('postgress: ',result);
		//console.log('RESULT  GENERAL MODE: '+result.rows[0].currentMode);
		callback(null,result.rows[0].currentMode)
		}
	})	
}

exports.setGeneralMode = function(locId, gmode, callback) {
	var queryString = 'INSERT INTO \"ModeSettings\" (\"modeSetId\", \"timestamp\", \"modeId\", \"locId\") VALUES (DEFAULT,localtimestamp,'+gmode+','+locId+');';
	console.log(queryString);
	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('postgress error',err);
			callback(err,null);
		}
		//console.log('postgress: ',result);
		callback(null,result);				
	})
}

exports.getSchedule = function(req, res) {
	var locId = req.params.roomId;
	if(locId){
		getGeneralMode(locId, function(err, gmode) {
			if (err) {
				console.log('getGneneralModeId err: '+err)
				res.send(500,err);
			}
			var queryString = 'SELECT * FROM \"Schedules\" s'+
				' WHERE \"locId\" = '+locId+' AND \"generalmodeId\"='+gmode+';';

			dbQuery(queryString, function(err,result) {
				if (err) {
					console.log('postgress error',err);
					res.send(500,err);
				}
				//console.log('query: ',queryString);
				//console.log('postgress: ',result);
				Schedule.toJsonSchedule(result, function(ofWeekIntervals) {
					var jsonSchedule = {
						heatjackId: 1,
						scheduleType: 0,
						generalMode: gmode,
						ofWeekIntervals: ofWeekIntervals
					};
					//console.log(jsonSchedule)
					res.writeHead(200, { 'Content-Type': 'application/json' });   
					res.write(JSON.stringify(jsonSchedule));
					res.end();	
				})
			})
		})
	}
	else {
		console.log('GetSchedule Error: locId not defined! ');
	}	
}

exports.postSchedule = function(req, res) {
	var locId = req.params.roomId;
	var schedule = req.body;
	getGeneralMode(locId, function(err, gmode) {
			if (err) {
				console.log('getGneneralModeId err: '+err)
				res.send(500,err);
			}
			else {
				//console.log('GeneralMode: '+gmode);
				dbQuery('DELETE FROM \"Schedules\" s WHERE s.\"locId\" = '+locId+' AND \"generalmodeId\" = '+gmode+';', function(err,result) {
					if (err) {
						console.log('postgress error',err);
						//res.send(500,err);
					}
					schedule.generalMode = gmode;
					Schedule.toPostgresSchedule(schedule, locId, function(data) {	
						var queryString = 'INSERT INTO \"Schedules\" (\"scheduleId\",\"locId\",\"begin\",\"end\",\"modeId\",\"name\",\"generalmodeId\") VALUES '+data+';';
						//console.log(queryString)
						dbQuery(queryString, function(err,result) {
							if (err) {
								console.log('postgress error',err);
								res.send(500,err);
							}
							//console.log('postgress: ',result);
							res.send(200);				
						})
					})			
				})	
			}
		})	
}

exports.postSetpoints = function(req, res) {
	var locId = req.params.roomId;
	var setpointComfort = req.body.setpointComfort;
	var setpointAway = req.body.setpointAway;
	var setpointNight = req.body.setpointNight;
	var queryString = 'INSERT INTO \"Settings\"'+
		' (\"settingsId\",\"locId\",\"setpointComfort\",\"setpointAway\",\"setpointNight\", \"timestamp\")'+
		' VALUES (DEFAULT,'+locId+','+setpointComfort+','+setpointAway+','+setpointNight+',localtimestamp);';
	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('postgress error',err);
			res.send(500,err);
		}
		//console.log('postgress: ',result);
		res.send(200);				
	});
}

exports.postSingleSetpoint = function(locId, setpoint, mode) {
	var queryString = 'SELECT \"setpointComfort\", \"setpointAway\", \"setpointNight\" FROM \"Settings\" '+
		' WHERE "locId" = '+locId+' ORDER BY \"settingsId\" DESC LIMIT 1;';
	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('postgress error',err);
			
		}
		//console.log(result.rows[0])
		var setpointComfort = result.rows[0].setpointComfort;
		var setpointNight = result.rows[0].setpointNight;
		var setpointAway = result.rows[0].setpointAway;
		switch(parseInt(mode))
		{
			case 0:
				setpointComfort = setpoint;
				break;
			case 1:
				setpointNight = setpoint;
				break;
			case 2:
				setpointAway = setpoint;
		}
		var queryString = 'INSERT INTO \"Settings\"'+
			' (\"settingsId\",\"locId\",\"setpointComfort\",\"setpointAway\",\"setpointNight\", \"timestamp\")'+
			' VALUES (DEFAULT,'+locId+','+setpointComfort+','+setpointAway+','+setpointNight+',localtimestamp);';
			//console.log(queryString);
		dbQuery(queryString, function(err,result) {
			if (err) {
				console.log('postgress error',err);
				
			}
			//console.log('postgress: ',result);				
		});
					
	});
	
}  	

exports.getRoomsWithActuatorsByUserId = function(userId, callback) {
	dbQuery('SELECT l.\"locId" as roomId FROM \"Locations\" l, \"Users\" u, \"UserLocationRel\" ul, \"Actuators\" a'+
		' WHERE u.\"userId\" = ul.\"userId\" AND l.\"locId\" = ul.\"locId\" AND l.\"locId\" = a.\"locId\"'+ 
		' AND u.\"userId\" = '+userId+';', function(err, result) {
		if (err) {
			console.log('getRoomByUserId error');
			callback(err,null);
		}
		else {
			//console.log(result.rows);
			callback(null,result.rows);
		}
	});
}	

exports.getScheduleByLocId = function(locId, callback) {
	getGeneralMode(locId, function(err, gmode) {
			if (err) {
				console.log('getGneneralModeId err: '+err)
				res.send(500,err);
			}
		var queryString = 'SELECT s.* FROM \"Schedules\" s'+
			' WHERE s.\"locId\" = '+locId+' AND \"generalmodeId\"='+gmode+';';

		dbQuery(queryString, function(err,result) {
			if (err) {
				console.log('getScheduleByLocId error');
				callback(err,null);
			}
			Schedule.toJsonSchedule(result, function(ofWeekIntervals) {
				var jsonSchedule = {
					heatjackId: 1,
					scheduleType: 0,
					generalMode: gmode,
					ofWeekIntervals: ofWeekIntervals
				};
				callback(null,jsonSchedule);
			})
		})
	})	
}

exports.getActuatorsByLocId = function(locId, callback) {
	var queryString = 'SELECT a.\"actId\", a.name FROM \"Actuators\" a'+
		' WHERE a.\"locId\" = '+locId+';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getActuatorsByLocId error');
			callback(err,null);
		}
		else {
			callback(null,result.rows);
		}
	})
}

exports.getHeatUpTime = function(locId, callback) {
	var queryString = 'SELECT s.\"temperature\" as temp, s.\"setpoint\"' + 
				' FROM \"status4\" s ' + 
				' WHERE s.\"locid\" ='+locId+';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getHeatUpTime error');
			callback(err,null);
		}
		else {
			console.log(typeof result.rows[0]);
			if (typeof result.rows[0] != 'undefined' && result.rows[0] !== null) {
				var dT = result.rows[0].setpoint - result.rows[0].temp;
				if (dT < 0.25) {
					callback(null,0)
				}
				else { 
					var heatUpSlope = 0.03;
					var heatUpTime = Math.round(dT/heatUpSlope);
					callback(null,heatUpTime);
				}
			}
			else {
				callback('Query has no result',null)
			}
		}
	})	
}

exports.getGatewayByUserId = function(userId, callback) {
	var queryString = 'SELECT g.\"gatewayId\"'+ 
				' FROM \"Gateways\" g, \"UserGatewayRel\" ug' + 
				' WHERE g.\"gatewayId\" = ug.\"gatewayId\" AND ug.\"userId\" = '+userId+';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getGatewaysByUserId error');
			callback(err,null);
		}
		else {
			callback(null,result.rows[0].gatewayId);
		}
	})	
}

exports.getGatewayByMac = function(mac, callback) {
	var queryString = 'SELECT g.\"gatewayId\"'+ 
				' FROM \"Gateways\" g' + 
				' WHERE g.\"mac\" = \''+mac+'\';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getGatewayByMac error');
			callback(err,null);
		}
		else {
			callback(null,result.rows[0].gatewayId);
		}
	})	
}

exports.getUsersByGatewayId = function(gatewayId, callback) {
	var queryString = 'SELECT ug.\"userId\"'+ 
				' FROM \"UserGatewayRel\" ug' + 
				' WHERE ug.\"gatewayId\" = '+gatewayId+';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getUsersByGatewayId error');
			callback(err,null);
		}
		else {
			//console.log('db: '+ JSON.stringify(result.rows));
			callback(null,result.rows);
		}
	})	
}

exports.getUsersByLocId = function(locId, callback) {
	var queryString = 'SELECT ul.\"userId\"'+ 
				' FROM \"UserLocationRel\" ul' + 
				' WHERE ul.\"locId\" = '+locId+';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getUsersByLocId error');
			callback(err,null);
		}
		else {
			//console.log('db: '+ result.rows);
			callback(null,result.rows);
		}
	})	
}

exports.getLocIdsByUserId = function(userId, callback) {
	var queryString = 'SELECT ul.\"locId\"'+ 
				' FROM \"UserLocationRel\" ul' + 
				' WHERE ul.\"userId\" = '+userId+';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getLocIdsByUserId error');
			callback(err,null);
		}
		else {
			//console.log('db: '+ result.rows);
			callback(null,result.rows);
		}
	})	
}

exports.getDistance = function(userId, callback) {
	var queryString = 'SELECT \"distance\"'+ 
				' FROM \"Users\"' + 
				' WHERE \"userId\" = '+userId+';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getDistance error');
			callback(err,null);
		}
		else {
			//console.log('db: '+ result.rows[0].distance);
			callback(null,result.rows[0].distance);
		}
	})	
}

/*
Update online status of gateways
*/

exports.updateOnlineState = function(gatewayId, callback) {
	var queryString = 'UPDATE \"Gateways\"'+ 
				' SET \"online\" = TRUE, \"timestamp\"= localtimestamp' + 
				' WHERE \"gatewayId\" = '+gatewayId+';';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('updateOnlineState error');
			callback(err,null);
		}
		else {
			callback(null,true);
		}
	})	
}

/* 
	Get online sstatus of gateways
	gatewayId = 0 gets status of all
*/

exports.getOnlineState = function(gatewayId, callback) {
	if (gatewayId == 0) {
		var queryString = 'SELECT \"gatewayId\", \"name\", CASE WHEN timestamp >= (localtimestamp - interval \'100 seconds\') THEN TRUE'+
					' ELSE FALSE END AS online FROM \"Gateways\" ORDER BY \"gatewayId\";';
	}
	else {
		var queryString = 'SELECT CASE WHEN timestamp >= (localtimestamp - interval \'100 seconds\') THEN TRUE ELSE FALSE END as online FROM \"Gateways\"'+ 
					' WHERE \"gatewayId\" = '+gatewayId+';';
	}
	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getOnlineState error');
			callback(err,null);
		}
		else {
			callback(null,result.rows);
		}
	})

}

/*
	IBeaconData	
*/

exports.addIBeaconData = function(req, res) {
	var userId = req.params.userId;
	var locId = req.params.locId;
	var iBeacon =  req.body;
	var time = new Date().getTime;
	if(iBeacon) {
		if(iBeacon.id) {
			var queryString = 'SELECT \"sensorId\" FROM \"Sensors\" WHERE \"locId\"='+locId+' AND \"unitTypeId\"=13;';
			dbQuery(queryString, function(err,result) {
				if (err) {
					console.log('Error while trying to fetch iBeacons sensorId')
				}
				else {
					var sensorId = result.rows[0];
					var value;
					if(iBeacon.status === 'INSIDE') {
						value = 1;
					}
					else if(iBeacon.status === 'OUTSIDE') {
						value = 0;
					}
					else {
						value = -1;
					}
					var time = new Date().getTime();
					console.log('Adding iBeacon with sensorId: ' + sensorId + ' with status ' + value);

					var data = '('+value+', '+time+','+sensorId+');';
					var queryString = 'INSERT INTO \"Measurements\" (\"reading\",\"timestamp\",\"sensorId\") VALUES '+data;
					//console.log('Query: ', queryString);
	        		dbQuery(queryString, function(err,result) {
	                	if (err) {
	                       		console.log('postgress error',err)
	                	}
					});	
				}
			})
		}
	}		
}

exports.addIBeaconDataGET = function(req, res) {
	var userId = req.params.userId;
	var locId = req.params.locId;
	var status = req.params.status;
	var time = new Date().getTime;
	if(iBeacon) {
		if(iBeacon.id) {
			var queryString = 'SELECT \"sensorId\" FROM \"Sensors\" WHERE \"locId\"='+locId+' AND \"unitTypeId\"=13;';
			dbQuery(queryString, function(err,result) {
				if (err) {
					console.log('Error while trying to fetch iBeacons sensorId')
				}
				else {
					var sensorId = result.rows[0];
					var value;
					if(status === 'inside') {
						value = 1;
					}
					else if(status === 'outside') {
						value = 0;
					}
					else {
						value = -1;
					}
					var time = new Date().getTime();
					console.log('Adding iBeacon with sensorId: ' + sensorId + ' with status ' + value);

					var data = '('+value+', '+time+','+sensorId+');';
					var queryString = 'INSERT INTO \"Measurements\" (\"reading\",\"timestamp\",\"sensorId\") VALUES '+data;
					//console.log('Query: ', queryString);
	        		dbQuery(queryString, function(err,result) {
	                	if (err) {
	                       		console.log('postgress error',err)
	                	}
					});	
				}
			})
		}
	}		
}



/* 
	GPS Locations
*/
exports.addGpsLocation = function(req, res) {
	var userId = req.params.userId;
	var latitude =  req.body.latitude;
	var longitude = req.body.longitude;
	var time = new Date().getTime();
	var queryString = 'INSERT INTO \"GPSLocations\"'+
			' (\"gpsId\",\"latitude\",\"longitude\",\"userId\",\"timestamp\")'+
			' VALUES (DEFAULT,'+latitude+','+longitude+','+userId+','+time+');';
		console.log('Add GPS Location: ' + queryString);
		dbQuery(queryString, function(err,result) {
			if (err) {
				console.log('postgress error',err);
				res.send(500,err);
			}
			//console.log('postgress: ',result);				
			res.send(200,'Ok');
		});
}


exports.getLocForUserAndCategory = function(userId, category, callback) {
dbQuery('SELECT l.\"locId\" ' + 
				' FROM \"Locations\" l, \"Users\" u, \"UserLocationRel\" ul ' + 
				' WHERE u.\"userId\" = '+userId+' AND l.\"category\"=\''+category+'\' AND u.\"userId\" = ul.\"userId\" AND l.\"locId\" = ul.\"locId\";',
				 function(err, result) 
				{
					if (err) {
						console.log('ERROR: '+err); 
						callback(null);
					}
					if(result) {
						if(result.rows[0]){
							//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
							callback(result.rows[0]);
						}
					}
					else {
						callback(null);
					}	
				});
}


exports.getAvgCo2PerMinute = function(locId, flagRise, callback) {
dbQuery('SELECT max(ppmPerMin) as co2PerMin FROM (SELECT (c.\"m1\"-c.\"m2\") / ((c.\"t2\"-c.\"t1\")/1000/60) as ppmPerMin ' +
 ' FROM \"Co2Procedures\" c, \"Sensors\" s, \"Locations\" l ' +
 ' WHERE l.\"locId\" = '+locId+' AND l.\"locId\" = s.\"locId\" AND s.\"sensorId\" = c.\"sensorId\" AND c.\"flagRise\"='+flagRise+' AND c.\"m1\" - c.\"m2\" > 750 ORDER BY c.\"t2\" DESC LIMIT 10) t',
		function(err,result) {

		if (err) {
						console.log('ERROR: '+err); 
						callback(null);
					}
					if(result) {
						if(result.rows[0]){
							//console.log('CONTENT: '+JSON.stringify(result.rows[0]));
							callback(result.rows[0]);
						}
					}
					else {
						callback(null);
					}	
		});
}


exports.getMinutesForOneDegree = function(locId, flagHeat, callback) {
dbQuery('SELECT avg(timeForOneDegree) as minPerDegree FROM (SELECT ((c.\"t2\"-c.\"t1\")/1000/60) / (c.\"temp2\"-c.\"temp1\") as timeForOneDegree FROM \"HCProcedures\" c, \"Sensors\" s, \"Locations\" l '+
	' WHERE l.\"locId\" ='+locId+' AND l.\"locId\" = s.\"locId\" AND s.\"sensorId\" = c.\"sensorId\" AND c.\"flagHeat\"='+flagHeat+' AND c.\"temp2\" - c.\"temp1\" >= 1 ORDER BY c.\"t2\" DESC LIMIT 10) t',
		function(err,result) {

		if (err) {
					console.log('-----------ERROR: '+err); 
					callback(null);
					}
					if(result) {
						if(result.rows[0]){
							//console.log('CONTENT: '+JSON.stringify('----------:' + result.rows[0].minperdegree));
							callback(result.rows[0]);
						}
					}
					else {
						callback(null);
					}	
		});
}

exports.getMinutesForOneDegreeByGatewayId = function(gatewayId, callback) {
	var queryString = 'SELECT \"locId\", ROUND(CAST(avg(timeForOneDegree) AS NUMERIC),0) as minPerDegree '+
			'FROM (SELECT ((c.\"t2\"-c.\"t1\")/1000/60) / (c.\"temp2\"-c.\"temp1\") as timeForOneDegree, l.\"locId\" as \"locId\" '+
				'FROM \"HCProcedures\" c, \"Sensors\" s, '+
					 '(SELECT DISTINCT ul.\"locId\" '+
						'FROM "UserLocationRel" ul, ( '+ 
											'SELECT ug.\"userId\" '+ 
												'FROM \"UserGatewayRel\" ug WHERE \"gatewayId\" = '+gatewayId+') AS userids '+
												'WHERE ul.\"userId\" =  userids.\"userId\") AS l '+
'WHERE l.\"locId\" = s.\"locId\" AND s.\"sensorId\" = c.\"sensorId\" AND c.\"flagHeat\"=TRUE AND c.\"temp2\" - c.\"temp1\" >= 1 '+
'ORDER BY c.\"t2\" DESC LIMIT 10) t '+
'GROUP BY \"locId\"';
	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getMinutesForOneDegreeByGatewayId error');
			callback(err,null);
		}
		else {
			//console.log('db: '+ result.rows);
			callback(null,result.rows);
		}
	})	
		
}


/*
	Database queries for analytics
*/
exports.getMeasurement = function(userId, locId, callback) {
	dbQuery('SELECT s.\"currentReading\" as value, s.\"unitTypeId\", l.\"category\"' + 
				' FROM \"Sensors\" s, \"Locations\" l ' + 
				' WHERE s.\"locId\"=l.\"locId\" AND s.\"locId\" = '+locId+';',
				 function(err, result) 
				{
					if (err) {
						console.log('ERROR: '+err); 
						callback(null);
					}	
					if(result.rows[0]) {
						console.log('CONTENT: '+JSON.stringify(result.rows[0]));
						var temperatureVal;
						var humidityVal;
						var co2Val;
						var categoryVal = result.rows[0].category;
						for (var i = result.rows.length - 1; i >= 0; i--) {
							if(result.rows[i].unitTypeId === 1){
								temperatureVal = result.rows[i].value;
							}
							else if(result.rows[i].unitTypeId === 2) {
								humidityVal = result.rows[i].value;
							}
							else if(result.rows[i].unitTypeId === 3) {
								co2Val = result.rows[i].value;
							}
							else {
								// Other values
							}
						};
						var measurement = {
							temperature: temperatureVal,
							humidity: humidityVal,
							co2: co2Val,
							category: categoryVal
						};
						//console.log('----Get Measurement: ' + measurement);
							callback(measurement);
					}
					else {
						callback(null);
					}
				})
}

/*
Get offset between netatmo and trv temperature
*/
exports.getTempOffsets = function(gatewayId, callback) {
	var queryString = 'SELECT trvs.\"locId\", trvs.\"actId\", trvs.actuator, ROUND(CAST((\"Sensors\".\"currentReading\"-trvs.temperature+trvs.\"offset\") AS NUMERIC),1) AS offset '+ 
						'FROM \"Sensors\", ( '+
							'SELECT DISTINCT \"Actuators\".\"actId\",\"Actuators\".\"name\" AS actuator, \"Actuators\".temperature,\"Actuators\".\"offset\", \"Actuators\".\"locId\" '+ 
								'FROM \"Actuators\",( '+
									'SELECT DISTINCT ul.\"locId\" '+ 
										'FROM \"UserLocationRel\" ul, ( '+ 
											'SELECT ug.\"userId\" '+ 
												'FROM \"UserGatewayRel\" ug WHERE \"gatewayId\" = '+gatewayId+') AS userids '+  
												'WHERE ul.\"userId\" =  userids.\"userId\") AS locs '+ 
													'WHERE \"Actuators\".\"locId\" = locs.\"locId\") AS trvs '+ 
														'WHERE \"Sensors\".\"locId\" = trvs.\"locId\" and \"Sensors\".\"unitTypeId\" = 1;';

	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getTempOffsets error');
			callback(err,null);
		}
		else {
			//console.log('db: '+ result.rows);
			callback(null,result.rows);
		}
	})	
}

/*
	Get average setpoint temperatures for a given room category (feedback screen)
*/
exports.getAvgSetpoints = function(req, res) {
	var locId = req.params.roomId;
	if (locId) {
		var queryString = 
				'select f.*, \"Settings\".* '+
				'from \"Settings\", '+
				'(select e.* '+ 
				'from \"Locations\" '+
				'join '+
				'(select \"Locations\".category, round(cast(AVG(d.\"setpointComfort\") as numeric),1) as avgComfort, round(cast(AVG(d.\"setpointAway\") as numeric),1) as avgAway, round(cast(AVG(d.\"setpointNight\") as numeric),1) as avgNight  '+
				'from (select '+
				   'a.* '+
				'from '+
				    '\"Settings\" a '+
				    'inner join '+ 
				        '(select \"locId\", max(\"settingsId\") as maxid from \"Settings\" group by \"locId\") as c on '+
				        'a.\"settingsId\" = c.maxid) as d, \"Locations\" '+
				'where '+
				'd.\"locId\" = \"Locations\".\"locId\" '+
				'group by \"Locations\".category) as e on '+
				'e.category = \"Locations\".category '+
				'where \"locId\" = '+locId+' ) f '+
				'where \"Settings\".\"locId\" = '+locId+ ' '+
				'order by "settingsId" desc '+
				'limit 1 ;';

		dbQuery(queryString, function(err,result) {
			if (err) {
				console.log('getAvgSetpoints error');
				res.send(500,'getAvgSetpoints error')
			}
			else {
				//console.log('db: '+ result.rows);
				res.jsonp(result.rows[0])
			}
		})
	}	
	else {
		console.log('getAvgSetpoints error: locId not defined');
		res.send(500,'getAvgSetpoints error: locId not defined')
	}		
}



/*
	Not used at the moment
*/
exports.getScheduleByUserId = function(userId, callback) {
	var queryString =  	'SELECT a.\"name\" AS actuator,'+ 
		' s.\"locId\",'+ 
		' s.\"begin\",'+
		' s.\"end\",'+
		' s.\"modeId\",'+ 
		' s.\"name\"'+
		' FROM \"Schedules\" s, \"Actuators\" a'+
		' WHERE EXISTS('+
			' SELECT * FROM \"Locations\" l'+
			' INNER JOIN \"UserLocationRel\" ul ON ul.\"locId\" = l.\"locId\"'+
			' WHERE ul.\"userId\"='+userId+' AND l.\"locId\" = s.\"locId\" AND l.\"locId\" = a.\"locId\");';
	dbQuery(queryString, function(err,result) {
		if (err) {
			console.log('getScheduleByUserId error');
			callback(err,null);
		}
		//console.log(result)
			callback(null,result.rows);
	})
}






