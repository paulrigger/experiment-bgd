var express = require('express'),
    http = require('http');

var app = express();
app.configure(function () {
  app.set('port', 8090);
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});
var server = http.createServer(app);
var io = require('socket.io').listen(server);

// Here the actual database requests are handled
var rg = require('./restgress.js');
var qh = require('./quantifiedart.js');
var an = require('./analytics.js');

var messenger = require('./messenger.js');

server.listen(app.get('port'));
io.set('log level',1); // set log level to get all debug messages
io.on('connection',function(socket){
  socket.emit('init',{msg:"test"});
  socket.on('identify', function(mac, fn) {
    rg.getGatewayByMac(mac, function(err,gatewayId) {
        if (err) {
            fn(null);
        }
        else {
            fn(gatewayId);
        }

    });
  });
  socket.on('trv_readings',function(readings) {
    rg.addActMeasurements(readings);
  });
  socket.on('tempOffsets', function(gatewayId, fn) {
    rg.getTempOffsets(gatewayId,function(err, objsOffsets) {
        console.log(JSON.stringify(objsOffsets));
        if (err) {
            console.log('Error in socket.on(\'tempOffsets\')');
            fn(objsOffsets);
        }
         else {
            fn(objsOffsets);
        }
    });
  });
  socket.on('preHeatSlopes', function(gatewayId, fn) {
    rg.getMinutesForOneDegreeByGatewayId(gatewayId, function(err, objsPreHeatSlopes) {
        console.log(JSON.stringify(objsPreHeatSlopes));
        if (err) {
            console.log('Error in socket.on(\'preHeatSlopes\')');
            fn(objsPreHeatSlopes);
        }
         else {
            fn(objsPreHeatSlopes);
        }    
    });
  });
});



/* 
    DISTANCE CHANGE NOTIFIER NEEDS TO BE REFACTORED
*/

var distChangeListener = rg.pgListenClient;
distChangeListener.connect();
distChangeListener.query('LISTEN "distance"');
distChangeListener.on('notification', function(msg) {
    console.log(msg.channel);
    console.log(msg.payload);
     if (msg.channel === 'distance') {
        if (msg.payload > 0) {
            var payload = (msg.payload).split(':');
            var userId = parseInt(payload[0]);
            var distance = parseFloat(payload[1]);
            console.log('DISTANCE userId:' +userId);
            rg.getGatewayByUserId(userId, function(err, gatewayId) {
                if (err) {
                    console.log('getGatewayByUserId Error: '+err);
                } 
                rg.getLocIdsByUserId(userId, function(err, objLocIds) {
                if (err) {
                    console.log('getLocIdsByUserId Error: '+err);
                }
                    objLocIds.forEach(function(objLocId) {
                        rg.getCurrentAutoAwayOffset(objLocId.locId, function(err,offset) {
                            if (err) {
                                console.log('getCurrentAutoAwayOffset Error: '+err);
                            }
                            if (offset !== null) {
                                data = {
                                    gatewayId: gatewayId,
                                    roomId: objLocId.locId,
                                    offset: offset  
                                };
                                //messenger.send('autoaway',data);    
                            }            
                        });
                    });    
                });
            });
        }
    }   
});
/***********************************************************************************/


messenger.receive(function(topic,message) {
    var gatewayId = topic[1];

    console.log('Message received with topic: '+topic+' and message: '+message);
    if (message == 'online' || message == 'request_update') {

        /* STILL ONE USER PER GATEWAY */
        rg.getUsersByGatewayId(gatewayId, function(err, objsUserId) {
            objsUserId.forEach(function(objUserId) {
                var userId = objUserId.userId;
                rg.getRoomsWithActuatorsByUserId(userId, function(err,rooms) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        rooms.forEach(function(room) {
                            rg.getGeneralMode(room.roomid, function(err, gmode) {
                                if (err) {
                                    console.log(err);
                                }
                                if (gmode ==2) {
                                    rg.getCurrentAutoAwayOffset(room.roomid, function(err,offset) {
                                        if (err) {
                                            console.log('getCurrentAutoAwayOffset Error: '+err);
                                        }
                                        if (offset !== null) {
                                            data = {
                                                gatewayId: gatewayId,
                                                roomId: room.roomid,
                                                offset: offset  
                                            };
                                            console.log(JSON.stringify(data));
                                            messenger.send('autoaway',data);    
                                        }            
                                    });
                                }
                            });
                            rg.getScheduleByLocId(room.roomid, function(err,schedule) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    var data = {
                                        gatewayId: gatewayId,
                                        roomId: room.roomid,
                                        body: schedule
                                    };
                                    messenger.send('schedule',data);
                                }
                            });
                            rg.getSetpoints(room.roomid, function(err,setpoints) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    var data = {
                                        gatewayId: gatewayId,
                                        roomId: room.roomid,
                                        body: setpoints
                                    };
                                    messenger.send('setpoints',data);
                                }
                            });
                            rg.getActuatorsByLocId(room.roomid, function(err,actuators) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    var names = [];
                                    actuators.forEach(function(actuator) {
                                        names.push({
                                            actId: actuator.actId,
                                            name: actuator.name
                                        });
                                    });
                                    var data = {
                                        gatewayId: gatewayId,
                                        roomId: room.roomid,
                                        body: names
                                    };
                                    messenger.send('actuators',data);
                                }
                            });
                        });
                    }
                });
            });
                
        });
       
    }

    else if (message == 'ping') {
        rg.updateOnlineState(gatewayId, function(err,res) {
            if (err) console.log('updateOnlineState Error: '+err);
        });
    }   
    if (topic[2] == 'setpoint') {
        var message = message.split(':');
        var locId = message[0];
        var mode = message[1];
        var setpoint = message[2];
        rg.postSingleSetpoint(locId, setpoint, mode);
    }
});



/*
	GET - REST Endpoints
*/
app.get('/api',function(req, res) {
	res.send(200,' Pauls API up and running...');
});


app.get('/api/getMeasurementfrom/:userId',qh.getMeasurement);

/*
	POST - REST Endpoints
*/



 app.post('/api/postmeasurement',qh.addMeasurement);

 
